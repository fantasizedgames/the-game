ig.module(
  'game.uiController'
)
.requires(
  'game.util'
)
.defines(function() {

  window.ftd.ui = {

    updateSelect : function( entity, reset )
    {
      // If reset is set, hide the select box.
      if( reset )
      {
        Sizzle( '.tower-select' )[0].setAttribute( 'style', 'display: none;' );
        Sizzle( '.creep-select' )[0].setAttribute( 'style', 'display: none;' );
        return;
      }

      if( entity instanceof EntityTower )
      {
        // Show select.
        Sizzle( '.tower-select' )[0].setAttribute( 'style', 'display: block;' );
        // Set values.
        Sizzle( '.name .value' )[0].innerHTML = entity.name;
        Sizzle( '.level .value' )[0].innerHTML = entity.level;
        Sizzle( '.rarity .value' )[0].innerHTML = entity.rarity;
        Sizzle( '.price .value' )[0].innerHTML = entity.price;
        Sizzle( '.damage .value' )[0].innerHTML = entity.minDamage + ' - ' + entity.maxDamage;
        Sizzle( '.range .value' )[0].innerHTML = entity.range;
        Sizzle( '.speed .value' )[0].innerHTML = entity.speed;
        var effects = entity.projectiles[0].effects;
        if( effects.length )
        {
          for( var i=0; i < effects.length; i++ )
          {
            if( effects[i].type == 'dot' )
            {
              var markup = '<p>DOT - ' + effects[i].value +
                ' ('+ effects[i].frequency+ '/' + effects[i].time + ')</p>';
              Sizzle( '.effects .value' )[0].innerHTML = markup;
            }
            if( effects[i].type == 'slow' )
            {
              var markup = '<p>Slow - ' + effects[i].value +
                '% (' + effects[i].time + ')</p>';
              Sizzle( '.effects .value' )[0].innerHTML += markup;
            }
          }
        } else {
          Sizzle( '.effects .value' )[0].innerHTML = '';
        }

        var actionLinks = Sizzle( '.bottom a' );
        if( entity.owner.clientId == ig.game.ioController.clientId )
        {
          for( var i=0; i < actionLinks.length; i++ )
          {
            actionLinks[i].setAttribute( 'style', 'display: block;' );
          }
          Sizzle( '.bottom .upgrade' )[0].onclick = (
            function ( tower )
            {
              return function()
              {
                var player = ftd.getCurrentPlayer();
                if( player.gold >= ftd.calculateUpgradePrice( tower ) )
                {
                  ig.game.ioController.upgradeTower( tower );
                }
              }
            }( entity )
          );
          Sizzle( '.bottom .sell' )[0].onclick = (
            function ( tower )
            {
              return function()
              {
                var player = ftd.getCurrentPlayer();
                if( player.gold >= ftd.calculateUpgradePrice( tower ) )
                {
                  ig.game.ioController.sellTower( tower );
                }
              }
            }( entity )
          );
        } else
        {
          for( var i=0; i < actionLinks.length; i++ )
          {
            actionLinks[i].setAttribute( 'style', 'display: none;' );
          }
        }
      }

      if( entity.preview )
      {
        // Show select.
        Sizzle( '.tower-select' )[0].setAttribute( 'style', 'display: block;' );
        // Set values.
        Sizzle( '.name .value' )[0].innerHTML = entity.name;
        Sizzle( '.level .value' )[0].innerHTML = entity.level;
        Sizzle( '.rarity .value' )[0].innerHTML = entity.rarity;
        Sizzle( '.price .value' )[0].innerHTML = entity.price;
        Sizzle( '.damage .value' )[0].innerHTML = entity.minDamage + ' - ' + entity.maxDamage;
        Sizzle( '.range .value' )[0].innerHTML = entity.range;
        Sizzle( '.speed .value' )[0].innerHTML = entity.speed;
        var effects = entity.projectiles[0].effects;
        if( effects.length )
        {
          for( var i=0; i < effects.length; i++ )
          {
            if( effects[i].type == 'dot' )
            {
              var markup = 'DOT - ' + effects[i].value +
                ' ('+ effects[i].frequency+ '/' + effects[i].time + ')';
              Sizzle( '.effects .value' )[0].innerHTML = markup;
            }
            if( effects[i].type == 'slow' )
            {
              var markup = '<p>Slow - ' + effects[i].value +
                '% (' + effects[i].time + ')</p>';
              Sizzle( '.effects .value' )[0].innerHTML += markup;
            }
          }
        } else {
          Sizzle( '.effects .value' )[0].innerHTML = '';
        }

        // Don't show actions buttons on tower previews.
        var actionLinks = Sizzle( '.bottom a' );
        for( var i=0; i < actionLinks.length; i++ )
        {
          actionLinks[i].setAttribute( 'style', 'display: none;' );
        }
      }

    },

    setTowerDeck : function( deck )
    {
      var towerSlots = Sizzle('.tower-deck .tower');
      for( var i=0; i < deck.length; i++ ) {
        towerSlots[i].onclick = (function (tower) {
          return function () {
            // Spawn only, if no other tower is selected.
            if(!ig.game.towerSelected) {
              ig.game.towerSelected = true;
              ig.game.spawnEntity(EntityTower, 40, 50, tower)
            }
          }
        })(deck[i]);
        // Add hover function.
        towerSlots[i].onmouseover = (function (tower) {
          return function () {
            tower.preview = true;
            ftd.ui.updateSelect( tower );
          }
        })(deck[i]);
        // Clear select, on mouseout.
        // Add hover function.
        towerSlots[i].onmouseout = (function (tower) {
          return function () {
            ftd.ui.updateSelect( null, true );
          }
        })(deck[i]);

        Sizzle('.tower-icon', towerSlots[i])[0].setAttribute('src', 'media/tower/' + deck[i].sprite);
        Sizzle('.tower-name', towerSlots[i])[0].innerHTML = deck[i].name;
      }
    },

    showScoreBoard : function( active )
    {
      if( active ) Sizzle( '.scoreboard' )[0].setAttribute( 'style', 'display: block;' );
      if( !active ) Sizzle( '.scoreboard' )[0].setAttribute( 'style', 'display: none;' );
    },

    updateScoreBoard : function()
    {
      var players = ig.game.gameCont.players;
      players.forEach( function( player, i )
      {
        Sizzle( '.scoreboard .players .player' )[i].setAttribute( 'style', 'display: block;' );
        var playerContainer = Sizzle( '.scoreboard .players .player' );
        Sizzle( '.name', playerContainer[i] )[0].innerHTML = player.name;
        Sizzle( '.gold', playerContainer[i] )[0].innerHTML = player.gold;
        Sizzle( '.kills', playerContainer[i] )[0].innerHTML = player.kills;
        Sizzle( '.score', playerContainer[i] )[0].innerHTML = player.score;
      });
    },

    setPlayerStat : function()
    {
      var player = ftd.getCurrentPlayer();
      Sizzle('.player-stats .life .value')[0].innerHTML = player.life;
      Sizzle('.player-stats .gold .value')[0].innerHTML = player.gold;
    },

    updatePlayerOverlay : function( players, gameInfo, kill )
    {
      if( kill )
      {
        var overlay = Sizzle( '.overlay' )[0];
        overlay.parentNode.removeChild( overlay );
        return;
      }

      players.forEach( function( player, i )
      {
        Sizzle( '.overlay .players .player' )[i].setAttribute( 'style', 'display: block;' );
        var playerContainer = Sizzle( '.overlay .players .player' );
        Sizzle( '.name', playerContainer[i] )[0].innerHTML = player.name;
        if( player.state == 'loading' )
        {
          Sizzle( '.state', playerContainer[i] )[0].innerHTML = '&#x2732';
        }
        else if( player.state == 'ready' )
        {
          var state = Sizzle( '.state.rotating', playerContainer[i] )[0];
          state.className = state.className.replace('rotating','ready');
          Sizzle( '.state', playerContainer[i] )[0].innerHTML = '&#x2713';
        }
      });

      var gameInfoContainer = Sizzle( '.overlay .game .info' );
      Sizzle( '.name span', gameInfoContainer[0] )[0].innerHTML = gameInfo.name;
      Sizzle( '.map span', gameInfoContainer[0] )[0].innerHTML = gameInfo.map;
      Sizzle( '.limit span', gameInfoContainer[0] )[0].innerHTML = gameInfo.playerLimit;
      Sizzle( '.mode span', gameInfoContainer[0] )[0].innerHTML = gameInfo.mode;
      Sizzle( '.difficulty span', gameInfoContainer[0] )[0].innerHTML = gameInfo.difficulty;

      // TODO: var gameImageContainer = Sizzle( '.overlay .game .info' );
    },

    updateWaveInfo : function( current, next )
    {
      Sizzle( '.wave-info .current' )[0].innerHTML = "<p>" + current[0].name + "</p>";
      Sizzle( '.wave-info .next' )[0].innerHTML = "<p>" + next[0].name + "</p>";
    },

    initializeChat : function()
    {
      // For whatever reason, this doesn't work without a timeout.
      setTimeout( function()
      {
        var chat = Sizzle( '.chat' )[0];
        // Clear chat window.
        Sizzle( '.received', chat )[0].innerHTML = '';
        // Enable submit button.
        Sizzle( 'form input[type="submit"]')[0].removeAttribute( 'disabled' );

        // Add submit listener.
        Sizzle( 'form', chat)[0].onsubmit = function( e )
        {
          e.preventDefault();
          var message = Sizzle( 'form input[type="text"]')[0].value;
          ig.game.ioController.sendChatMessage( message );
          // Reset text field, after message is send to server.
          Sizzle( 'form input[type="text"]')[0].value = '';
        }
      }, 1);
    },

    processChatMessage : function( message )
    {
      // Get chat container.
      var chat = Sizzle( '.chat .received' )[0];

      // Get sender.
      var sender;
      if( message.sender == 'Game server' )
      {
        sender = { name : 'Game Server', color : 'game-server'}
      }else {
        sender = ftd.getPlayerByClientId( message.sender );
      }

      // Create new div for the message.
      var container = document.createElement( 'div' );
      container.innerHTML = '<div class="author">' + sender.name + ':</div><div class="message"> ' + message.message + '</div>';
      container.className = 'message-container color-' + sender.color;

      chat.appendChild( container );
    }
  };
});
