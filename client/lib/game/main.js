ig.module(
  'game.main'
)
.requires(
  'impact.game',
  'game.uiController',
  'game.ioController',
  'impact.font',
  'game.util',
  'game.uiController',
  'game.entities.creep',
  'game.entities.tower',
  'game.entities.gameController',
  'impact.debug.debug',
  'plugins.valueMap',
  'plugins.impact-splash-loader',
  'plugins.astar-for-entities',
  'game.levels.wastelands',
  'game.levels.footsteps',
  'game.levels.greatgrass',
  'game.levels.iceagony'
)
.defines( function()
{
  // We need to initialize the connection, before game is loaded.
  var socket = io.connect( 'http://localhost:5429' );
  // Set game properties.
  var playerId = ftd.getCookie( 'guest' ).id;
  var gameId = ftd.getQueryString( 'gid' );

  // Initialize io socket controller.
  var ioController = new IoController( socket, playerId, gameId );

  MyGame = ig.Game.extend({

    tile_size : 40,
    gameCont: null,

    // Load a fonts
    font: new ig.Font( 'media/default.font.png' ),
    bigFont: new ig.Font( 'media/halfbig.font.png' ),
    whiteFont: new ig.Font( 'media/white.font.png' ),
    yellowSmallFont : new ig.Font( 'media/fonts/yellow.font.png' ),
    greenFont: new ig.Font( 'media/green.font.png'),

    init : function()
    {
      // Call ready.
      this.ioController = ioController;
      this.ioController.ready();

      // Auto sort zindes on entities.
      ig.game.autoSort = true;

      // Define game keybind.
      ig.input.bind( ig.KEY.ESC, 'escape' );
      ig.input.bind( ig.KEY.MOUSE1, "CanvasTouch" );
      // Tower keybinds
      ig.input.bind( ig.KEY._1, 1 );
      ig.input.bind( ig.KEY._2, 2 );
      ig.input.bind( ig.KEY._3, 3 );
      ig.input.bind( ig.KEY._4, 4 );
      ig.input.bind( ig.KEY._5, 5 );
      ig.input.bind( ig.KEY._6, 6 );
      // Camera movement keybinds
      ig.input.bind( ig.KEY.LEFT_ARROW, 'left' );
      ig.input.bind( ig.KEY.RIGHT_ARROW, 'right' );
      ig.input.bind( ig.KEY.UP_ARROW, 'up' );
      ig.input.bind( ig.KEY.DOWN_ARROW, 'down' );
      // Scoreboard keybind
      ig.input.bind( ig.KEY.TAB, 'showScoreboard' );
    },

    loadLevel : function ( level )
    {
      this.parent( level );
      // Add value layer to globals.
      var vl = ig.game.getMapByName( 'valueLayer' );
      vl.visible = false;
      this.vMap = new ig.ValueMap( vl.tilesize, vl.data, false );
      this.vMap.removeCollision = false;
      // Hide value map.
      ig.BackgroundMap.inject({
        visible:true,
        draw : function ()
        {
          if ( !this.visible ) return;
          this.parent();
        }
      });

      // Initialize game controller.
      this.gameCont = ig.game.spawnEntity( EntityGameController, 0, 0 );
    },

    update : function()
    {
      // Call camera function
      this.scrollScreen();
      // Toggle ingame scoreboard
      if( ig.input.state('showScoreboard') ) ftd.ui.showScoreBoard( true );
      if( ig.input.released('showScoreboard') ) ftd.ui.showScoreBoard( false );
      // Check if a tower keybind is presset.
      //this.towerKeysListen();
      this.parent();
    },

    towerKeysListen : function()
    {
      var towerCount = ig.game.deck.length + 1;
      for(var i = 1; i < towerCount; i++)
      {
        // Check each possible numeric key.
        if(ig.input.pressed(i))
        {
          // Spawn only, if no other tower is selected.
          if(!ig.game.towerSelected)
          {
            ig.game.towerSelected = true;
            ig.game.spawnEntity(EntityTower, 40, 50, ig.game.deck[i-1]);
          }
        }
      }
    },

    scrollScreen : function()
    {
      var gameWidth  = ig.game.collisionMap.width*ig.game.collisionMap.tilesize;
      var gameHeight = ig.game.collisionMap.height*ig.game.collisionMap.tilesize;

      var gameBorderTop    = 0;
      var gameBorderRight  = gameWidth - ig.system.width;
      var gameBorderBottom = gameHeight - ig.system.height;
      var gameBorderLeft   = 0;

      // Camera Movement Speed in pixels per update while input is in state,
      // 12 pixels movement, 60 times per second = 720 pixels/second.
      var cameraMoveSpeed = 20;

      if( ig.input.state('left') ) this.screen.x -= cameraMoveSpeed;
      if( ig.input.state('right') ) this.screen.x += cameraMoveSpeed;
      if( ig.input.state('up') ) this.screen.y -= cameraMoveSpeed;
      if( ig.input.state('down') ) this.screen.y += cameraMoveSpeed;

      // Left scroll - make sure it stops when the map stops.. map x start
      if( ig.input.state('left')
        && !this.screen.x <= gameBorderLeft )
      {
        if( this.screen.x > gameBorderLeft )
        {
          this.screen.x -= cameraMoveSpeed;
        }
        else {
          this.screen.x = gameBorderLeft;
        }
      }

      // Up scroll - make sure it stops when the map stops.. map y start
      if( ig.input.state('up')
        && !this.screen.y <= gameBorderTop )
      {
        if( this.screen.y > gameBorderTop )
        {
          this.screen.y -= cameraMoveSpeed;
        }
        else {
          this.screen.y = gameBorderTop;
        }
      }

      // (ig.game.collisionMap.width*ig.game.collisionMap.tilesize-ig.system.width) is end of map.. FOR X / WIDTH
      if( ig.input.state('right')
        && !this.screen.x <= gameBorderRight / 2 )
      {
        if( this.screen.x < gameBorderRight )
        {
          this.screen.x += cameraMoveSpeed;
        }
        else {
          this.screen.x = gameBorderRight;
        }
      }

      // (ig.game.collisionMap.height*ig.game.collisionMap.tilesize-ig.system.height) is end of map.. FOR Y / HEIGHT
      if( ig.input.state('down')
        && !this.screen.y <= gameBorderBottom / 2 )
      {
        if(this.screen.y < gameBorderBottom )
        {
          this.screen.y += cameraMoveSpeed;
        }
        else {
          this.screen.y = gameBorderBottom;
        }
      }
    }

  });

  // Start the Game with 60fps, a resolution of 960 x 640, real scale, biolab impact loader
  ig.main( '#canvas', MyGame, 60, 880, 480, 1, ig.ImpactSplashLoader );
});
