ig.module(
  'game.util'
)
.requires()
.defines(function() {

  window.ftd = {

    /**
     * Entity functions.
     */

    getCenter : function( pos, size ) {
      var center = {};
      center.x = pos.x + (size.x - (size.x / 2));
      center.y = pos.y + (size.y - (size.y / 2));
      return center;
    },

    cursorInside : function( pos, size ) {
      var inside = true;
      size = typeof size !== 'undefined' ? size : {x : 0, y : 0 };
      // Check if the cursor is outside the width (from x start, to x end.)
      if( ig.input.mouse.x + ig.game.screen.x <= pos.x
          || ig.input.mouse.x + ig.game.screen.x >= pos.x + size.x )
      {
        inside = false;
      }
      // Check if the cursor is outside the height (from y start, to y end.)
      if( ig.input.mouse.y + ig.game.screen.y <= pos.y
          || ig.input.mouse.y + ig.game.screen.y >= pos.y + size.y )
      {
        inside = false;
      }
      return inside;
    },

    calculateUpgradePrice : function( tower )
    {
      var price = (tower.price / 100) * tower.upgradePercentage;
      return Math.ceil( price );
    },

    /**
     * Tiles function.
     */

    getTilePositions : function( x, y, size ) {
      var tile = {};
      tile.x = ( (x / ig.game.tile_size).round() * ig.game.tile_size ) + ig.game.screen.x;
      tile.y = ( (y / ig.game.tile_size).round() * ig.game.tile_size - (size.y / 2) ) + ig.game.screen.y;
      return tile;
    },

    getTile : function( x, y ) {
      var xRounded = (x / ig.game.tile_size).round() * ig.game.tile_size;
      var yRounded = (y / ig.game.tile_size).round() * ig.game.tile_size;
      return xRounded + yRounded;
    },

    getValueOnTile : function( x, y ) {
      // Find center, by dividing x/y with tile size.
      return ig.game.vMap.getValue( x + (ig.game.tile_size / 2), y + (ig.game.tile_size / 2) );
    },

    /**
     * Find by functions.
     */

    getEntityById : function(entity, id) {
      if(entity == 'tower') {
        for(var i=0; i < ig.game.gameCont.activeTowers.length; i++) {
          if(ig.game.gameCont.activeTowers[i].id == id) {
            return ig.game.gameCont.activeTowers[i];
          }
        }
      }
      else if(entity == 'creep') {
        for(var i=0; i < ig.game.gameCont.activeCreeps.length;i++) {
          if(ig.game.gameCont.activeCreeps[i].id == id) {
            return ig.game.gameCont.activeCreeps[i];
          }
        }
      }
      return false;
    },

    setPlayerPropById : function( clientId, property, value ) {
      var players = ig.game.gameCont.players;
      for( var i=0; i < players.length; i++ ) {
        if(players[i].clientId == clientId
          || clientId == 'all')
        {
          if(property == 'gold') players[i].gold += value;
          if(property == 'score') players[i].score += value;
          if(property == 'kills') players[i].kills += value;
          if(property == 'life') players[i].life += value;
        };
      }
    },

    getPlayerPropById : function( clientId, property ) {
      var players = ig.game.gameCont.players;
      for( var i=0; i < players.length; i++ ) {
        if( players[i].clientId == clientId )
        {
          if(property == 'gold') return players[i].gold;
          if(property == 'score') return players[i].score;
          if(property == 'kills') return players[i].kills;
          if(property == 'life') return players[i].life;
        };
      }
    },

    getPlayerByClientId : function( clientId )
    {
      var players = ig.game.gameCont.players;
      for( var i=0; i < players.length; i++ ) {
        if(players[i].clientId == clientId) {
          return players[i];
        };
      }
    },

    getCurrentPlayer : function()
    {
      var clientId = ig.game.ioController.clientId;
      var players = ig.game.gameCont.players;
      for( var i = 0; i < players.length; i++ ) {
        if( players[i].clientId == clientId ) {
          return players[i];
        }
      }
    },

    getRealPlayerColor : function( color )
    {
      var realColor;
      if( color == 0 ) realColor = "#ea5050";
      if( color == 1 ) realColor = "#51b3ff";
      if( color == 2 ) realColor = "#55ba4a";
      if( color == 3 ) realColor = "#decf25";
      if( color == 4 ) realColor = "#a18147";
      if( color == 5 ) realColor = "#be69c8";
      if( color == 6 ) realColor = "#f29208";
      if( color == 7 ) realColor = "#38c6c6";
      return realColor;
    },

    /**
     * Misc functions
     */

    getCookie : function( name )
    {
      var cookies = document.cookie.split( ";" );
      for( i=0; i < cookies.length; i++ )
      {
        var key = cookies[i].substr( 0, cookies[i].indexOf( "=" ) );
        var value = cookies[i].substr( cookies[i].indexOf( "=" ) +1 );
        // Kill white space.
        key = key.replace( /^\s+|\s+$/g, "" );
        if( key == name )
        {
          eval( 'var cookie = ' +  unescape( value ) );
          return cookie;
        }
      }
    },

    getQueryString :function(name)
    {
      name = name.replace( /[\[]/, "\\\[" ).replace( /[\]]/, "\\\]" );
      var regexS = "[\\?&]" + name + "=([^&#]*)";
      var regex = new RegExp( regexS );
      var results = regex.exec( window.location.search );
      if( results == null )
      {
        return "";
      } else {
        return decodeURIComponent(results[1].replace(/\+/g, " "));
      }
    }
  };
});
