ig.module(
  'game.entities.spawner'
)
  .requires(
    'impact.entity','game.entities.creep'
  )
  .defines(function() {

    EntitySpawner = ig.Entity.extend({

      name:'spawner',
      size: {x: 40, y: 40},
      waves: null,
      remainingWaves: true,
      spreadTimer: new ig.Timer(0.5),
      queready:false,
      creepQueue: [],
      nextWave : 0,

      type: ig.Entity.TYPE.NONE,
      checkAgainst: ig.Entity.TYPE.NONE,
      collides: ig.Entity.COLLIDES.NONE,

      /**
       * Spawner properties.
       *
       * size -> Object containing size properties,
       * waves -> Waves for the current level loaded from main,
       * creepsettings -> All creep settings loaded from main - Needs to be level specific
       * currentcreepsettings -> Settings object for the current creep beeing spawned
       * creepque -> Object Array for creep id's and spread or the wave in progress.
       * nextWave -> Integer counter for waves,
       */

      init: function( x, y, settings ) {
        this.parent( x, y, settings );
      },

      update: function(){
        this.spawnCreep();
        this.parent();
      },

      spawnCreep: function() {
        // Spawn from creep queue.
        if(this.creepQueue.length > 0 && this.spreadTimer.delta() >= 0) {
          var creep = this.creepQueue.pop();
          this.spreadTimer = new ig.Timer(creep.spread);
          ig.game.spawnEntity(EntityCreep, this.pos.x, this.pos.y, creep);
        }
      },

      queueCreep : function(creep) {
        this.creepQueue.push(creep);
      }
    });
  });
