ig.module(
  'game.entities.projectile'
).requires(
  'impact.entity'
).defines(function(){

  EntityProjectile = ig.Entity.extend({

    // Set some of the properties
    collides: ig.Entity.COLLIDES.PASSIVE,
    maxVel: {x: 500, y: 500},
    flip: false,
    color: '#000',
    timer: new ig.Timer(),
    // zIndex at top, so that it dont flicker
    zIndex: 5,

    // projectile properties

    /**
     * speed: time passes before hitting the target
     * sprite: sprite of projectile,
     * anim: array of animations,
     * anim.name -> name of animation
     * anim.time -> time between sprite switch
     * anim.seq -> sequence of animation
    **/

    init : function( x, y, settings ) {
      this.parent( x, y, settings );
      this.animSheet = new ig.AnimationSheet( 'media/projectile/' + this.sprite, this.size.x, this.size.y );
      if( !ig.global.wm )
      {
        // Add each animation.
        for( var i = 0; i < this.animations.length; i++ )
        {
          this.addAnim( this.animations[i].name, this.animations[i].time, this.animations[i].sequence );
        }
      }
      // Initial animation.
      this.currentAnim = this.anims.move;
    },

    update : function() {
      this.travel();
      this.parent();
    },

    travel : function()
    {
      var center = ftd.getCenter( this.pos, this.size );
      var pCenterX = center.x;
      var pCenterY = center.y;

      var creepCenter = ftd.getCenter( this.target.pos, this.target.size );
      var creepCenterX = creepCenter.x;
      var creepCenterY = creepCenter.y;

      var xDist = creepCenterX - pCenterX;
      var yDist = creepCenterY - pCenterY;

      var travelTime = (xDist * xDist) + (yDist * yDist);
      var cFact = 100000 / travelTime;

      this.vel.x = Math.sqrt(cFact) * xDist;
      this.vel.y = Math.sqrt(cFact) * yDist;

      // + (90).toRad() to compensate for the projectile image pointing north
      this.currentAnim.angle = this.angleTo( this.target ) + (90).toRad();

      // 3 is just to make center of target aim a little bigger
      if( (pCenterX <= creepCenterX + 3)
          && (pCenterY <= creepCenterY + 3)
          && (pCenterX >= creepCenterX - 3)
          && (pCenterY >= creepCenterY - 3) )
      {

        if( this.timer ) {
          // TODO: Should be ig.timer, not counting in frames.

          this.currentAnim = this.anims.dead;
          // Remove health from creep, with a roll from tower damage
          this.target.health -= this.damage;
          // Add damage message.
          this.target.addDamageMessage( this.damage, false );

          // Add projectile effects
          if( this.effects )
          {
            for( var i=0; i < this.effects.length; i++ )
            {
              this.target.addEffect(this.effects[i]);
            }
          }
        }
        if( this.timer )
        {
          this.kill();
        }
      }
    },

    handleMovementTrace: function( res ) {
      // IGNORE COLLISION TILES
      this.pos.x += this.vel.x * ig.system.tick;
      this.pos.y += this.vel.y * ig.system.tick;
    }
  });
});
