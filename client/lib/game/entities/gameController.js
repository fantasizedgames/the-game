ig.module(
  'game.entities.gameController'
)
.requires(
  'impact.entity',
  'game.entities.spawner',
  'plugins.minimap'
)
.defines(function(){

  EntityGameController = ig.Entity.extend({

    name :'gameCont',
    loadLength : 10,
    type : ig.Entity.TYPE.NONE,
    checkAgainst : ig.Entity.TYPE.NONE,
    collides : ig.Entity.COLLIDES.NONE,

    minimap: null,
    players : [],
    activeTowers : [],
    activeCreeps : [],
    secTimer : null,
    curSec : 0,
    curMin : 0,
    statusMessages : [],
    damageMessages : [],

    init : function(x, y, data) {
      this.parent(x, y, data);
      this.secTimer = new ig.Timer(1);
      this.spawners = ig.game.getEntitiesByType( EntitySpawner );
    },


    setStartScreen : function()
    {
      var player = ftd.getCurrentPlayer();
      for( var i = 0; i < this.spawners.length; i++ )
      {
        var spawner = this.spawners[i];
        if( spawner.color == player.color )
        {
          // set startscreen position according to players spawner
          ig.game.screen.x = spawner.pos.x - (ig.system.width / 2);
          ig.game.screen.y = spawner.pos.y - (ig.system.height / 2);
        }
      }
    },

    addCreep : function( creep )
    {
      this.spawners.forEach( function( spawner )
      {
        // Spawn creep only in their rightful spawner.
        if( spawner.color == creep.spawn )
        {
          spawner.queueCreep( creep );
        }
      });
    },

    setGameState : function(state) {
      this.state = state;
      // handle states
    },

    appendLife : function(life) {
      this.life += life;
    },

    appendGold : function(gold) {
      if(this.gold + gold < 0) {
        // TODO: Spawn no gold message
        return false;
      }
      this.gold += gold;
      return true;
    },

    //Increments UI clock and globaltimer
    setTime : function() {

    	if(this.secTimer.delta() >0){
    		this.curSec ++;
    		this.secTimer.reset();
    	}
    	if (this.curSec==60){
    		this.curSec=0;
    		this.curMin ++;
    	}
    },
    // returns a string with game clock.
    getElapsedTime : function() {
    	return this.curMin +" : "+ this.curSec;
    },

    // Should be moved to util class.
    addMessage : function(message, pos, timeInFrames) {
      this.statusMessages.push({message : message, pos : pos, frames : timeInFrames });
    },
    // Should be moved to util class.
    drawMessages : function() {
      if(this.statusMessages.length > 0) {

        for(var i=0; i < this.statusMessages.length; i++) {
          // Animate up
          this.statusMessages[i].pos.y--;
          // Draw, with information from statusMessage array
          ig.game.bigFont.draw(this.statusMessages[i].message, this.statusMessages[i].pos.x - ig.game.screen.x + 1, this.statusMessages[i].pos.y - ig.game.screen.y - 7);
          this.statusMessages[i].frames--;
          if(this.statusMessages[i].frames < 1)  this.statusMessages.splice(0, 1);
        }
      }
    },

    drawDamageMessages : function()
    {
      var self = this;
      this.damageMessages.forEach( function( message, i )
      {
        if( message.showTimer.delta() <= 0 )
        {
          if( !message.critical )
          {
            ig.game.yellowSmallFont.draw( message.content, message.pos.x - ig.game.screen.x, message.pos.y - ig.game.screen.y);
            // Slowly levitate.
            message.pos.y -= 0.5;
          }
        } else {
          delete self.damageMessages[i];
        }
      });
    },

    loadMinimap : function( map ) {
      var gameX = (ig.game.collisionMap.width*ig.game.collisionMap.tilesize);
      var gameY = (ig.game.collisionMap.height*ig.game.collisionMap.tilesize);
      this.minimap = new ig.Minimap( 10, 10, 120, 120, gameX, gameY, map+'_minimap' );
    },

    draw : function() {
      this.drawMessages();
      // To update the minimap continously
      this.minimap.draw();
      // Draw damage messages.
      this.drawDamageMessages();
    },

    update : function()
    {
      this.parent();
    },

    start : function() {
      this.secTimer.reset();
    }
  });
});
