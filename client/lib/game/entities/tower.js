ig.module(
  'game.entities.tower'
).requires(
  'impact.entity',
  'game.entities.projectile',
  'game.entities.overlay'

).defines(function(){

  EntityTower = ig.Entity.extend({

    collides: ig.Entity.COLLIDES.PASSIVE,

    // @Param "name" : Tower name (+ number for upgraded)
    // @Param "level" : Tower Level (To calculate loot from)
    // @Param "description" : Tower description (to change on upgrade?)
    // @Param "level" : Tower level (To calculate loot from)
    // @Param "rarity" : Tower rarity, Bronze, Silver, Gold (To calculate loot from)
    // @Param "minDamage" : Tower minimum damage
    // @Param "maxDamage" : Tower maximum damage
    // @Param "range" : Tower range (transformed later to radians)
    // @Param "speed" : Tower speed (Its added speed variable)
    // @Param "price" : Tower price (changes at upgrade)
    // @Param "upgradePercentage" : Tower Upgrade percentage per. upgrade

    flip: false,
    state : "notPlaced",
    radiusColor: '10, 10, 10',
    targetCreep : null,
    zIndex : 2,
    fireQueue : [],
    clicked : false,
    selected : false,

    init : function( x, y, settings )
    {
      this.parent( x, y, settings );
      this.timer = new ig.Timer();
      this.animSheet = new ig.AnimationSheet('media/tower/'+ this.sprite, this.size.x, this.size.y);

      if(!ig.global.wm) {
        // Add all animations.
        for(var i=0; i < this.animations.length; i++) {
          this.addAnim( this.animations[i].name, this.animations[i].time, this.animations[i].sequence);
        }
      }
      // Set center positions of tower.
      this.center = ftd.getCenter( this.pos, this.size );

      // Set flag color, of tower.
      if( !this.owner )
      {
        this.flagColor = (ftd.getCurrentPlayer()).color;
      }else {
        this.flagColor = (ftd.getPlayerByClientId( this.owner.clientId )).color;
      }
    },

    update : function()
    {
      // Handle states of tower.
      this.maintainStates();
      this.center = ftd.getCenter( this.pos, this.size );
      this.parent();
    },

    maintainStates : function()
    {
      // Calculate current tile, used for snapping.
      var tile = ftd.getTilePositions(ig.input.mouse.x, ig.input.mouse.y, this.size);
      // Placing.
      if( this.state == 'notPlaced'
        && !ig.input.pressed('CanvasTouch') )
      {
        // Follow mouse.
        this.pos = tile;
        // Remove cursor while placing.
        document.body.style.cursor = "none";
        // If tower cancels, remove tower, and bring back cursor.
        if( ig.input.pressed("escape") )
        {
          document.body.style.cursor = "url(media/dom_ui/swordcursor_default.png), auto";
          ig.game.gameCont.addMessage(this.name + ' cancelled ', {'x':this.pos.x,'y': this.pos.y}, 1000);
          this.kill();
        }
      }
      // Place.
      else if( this.state == 'notPlaced'
              && ig.input.pressed('CanvasTouch')
              && this.clicked
              && ftd.getValueOnTile( ig.input.mouse.x + ig.game.screen.x, ig.input.mouse.y + ig.game.screen.y ) == -1 )
      {
        this.pos = tile;
        this.center = ftd.getCenter( this.pos, this.size );
        // Check if player got the gold, only for feedback purpose.
        if( !ig.game.gameCont.appendGold(-this.price) )
        {
          return false;
        }
        // Remove the tower, as it will be set, once the server have approved.
        this.kill();
        ig.game.ioController.placeTower(this);
        // Reverse game state.
        ig.game.towerSelected = false;
        // Bring back cursor.
        document.body.style.cursor = "url(media/dom_ui/swordcursor_default.png), auto";
      }
      // Select.
      if( this.state == 'placed'
          && ig.input.pressed('CanvasTouch')
          && ftd.cursorInside( this.pos, this.size ) )
      {
        if ( this.owner.clientId == ig.game.ioController.clientId )
        {
          ftd.ui.updateSelect( this );
        } else {
          // Alter DOM, without ownership GUI.
        }
        this.state = 'selected';
      }

      if( ig.input.pressed( 'escape' ) )
      {
        this.state = 'placed';
        ftd.ui.updateSelect( null, true );
      }

      // Deployed.
      if( this.state != 'notPlaced'
          && this.fireQueue.length > 0
          && ftd.getEntityById('creep', this.fireQueue[0].target)) // Check if creep exist yet.
      {
        // Start shooting.
        this.pickTarget();
      }
      this.clicked = true;
    },

    pickTarget : function()
    {
      // No need to run the loop in every update, if tower won't shoot anyway.
      if( this.timer.delta() >= this.speed )
      {
        this.timer.reset();
        var fire = this.fireQueue.shift(); // returns first thing in array.
        this.targetCreep = ftd.getEntityById( 'creep', fire.target );

        // Fire at creep, with speed of tower.
        this.launchProjectile( this.targetCreep, fire.damage, fire.effects );
      }
    },

    drawFlag : function()
    {
      // Add playercolor-flag animation effect.
      var bannerImg = new ig.Image( 'media/banner/banner_' + this.flagColor + '.png' );
      bannerImg.zIndex = 3;
      var _x = ig.system.getDrawPos(this.pos.x) - ig.game.screen.x + (this.size.x - bannerImg.width);
      var _y = ig.system.getDrawPos(this.pos.y) - ig.game.screen.y + (this.size.y - bannerImg.height);
      bannerImg.draw( _x, _y );
    },

    upgradeTower : function( upgrades )
    {
      // Upgrade tower properties.
      this.name = upgrades.name;
      this.minDamage = upgrades.minDamage;
      this.maxDamage = upgrades.maxDamage;
      this.range = upgrades.range;
      this.speed = upgrades.speed;
      this.price = upgrades.price;
      this.projectiles = upgrades.projectiles;

      // Check if there is a new sprite available for this upgrade.
      if( this.sprite.length > 1 )
      {
        for( var i=0; this.sprite.length > this.sprite.length; i++ )
        {
          if( this.sprite[i].level == this.upgradeCount )
          {
            this.animSheet = new ig.AnimationSheet( 'media/tower/' + this.sprite[i], this.size.x, this.size.y );
            break;
          }
        }
      }
      // Add upgrade animation effect.
      var settings = {
        sprite : 'upgradeanimation.png',
        frameTime : 0.1,
        runs : 1,
        seq : [0,1,2,3,4,5,6,7],
        size : {x:40, y:80},
        pos : {x : this.pos.x, y: this.pos.y}
      };
      ig.game.spawnEntity(EntityOverlay, this.pos.x, this.pos.y, settings);

      // Update select frame.
      ftd.ui.updateSelect( this );
    },

    sellTower : function()
    {
      ig.game.vMap.setValue(this.pos.x + (ig.game.tile_size / 2), this.pos.y + ig.game.tile_size, -1);
      // Add sell effect
      var settings = {
        sprite : 'sellanimation.png',
        frameTime : 0.1,
        runs : 1,
        seq : [0,1,2,3,4,5,6,7],
        size : {x:40, y:80},
        pos : {x : this.pos.x, y: this.pos.y}
      };
      ig.game.spawnEntity( EntityOverlay, this.pos.x, this.pos.y, settings );
      ig.game.towerSelected = false;
      // Kill and remove from active towers.
      var index = ig.game.gameCont.activeTowers.indexOf( this );
      ig.game.gameCont.activeTowers.splice( index, 1 );
      this.kill();
    },

    setShootArea : function()
    {
      var ctx = ig.system.context;
      ctx.strokeStyle = this.radiusColor;
      ctx.beginPath();
      ctx.arc( ig.system.getDrawPos(this.center.x) - ig.game.screen.x, ig.system.getDrawPos(this.center.y) - ig.game.screen.y, this.range, 0, Math.PI * 2);
      ctx.stroke();
      ctx.fillStyle = "rgba("+this.color+", 0.2)";
      ctx.closePath();
      ctx.fill();
    },

    setUpgradeShootArea : function()
    {
      var ctx = ig.system.context;
      ctx.strokeStyle = this.radiusColor;
      ctx.beginPath();
      ctx.arc( ig.system.getDrawPos(this.center.x) - ig.game.screen.x, ig.system.getDrawPos(this.center.y) - ig.game.screen.y, this.range + Math.ceil((this.range / 100) * this.upgradePercentage), 0, Math.PI * 2);
      ctx.stroke();
      ctx.fillStyle = "rgba("+this.color+", 0.3)";
      ctx.closePath();
      ctx.fill();
    },

    colorTile : function()
    {
      var ctx = ig.system.context;
      ctx.beginPath();
      ctx.rect( ig.system.getDrawPos(this.pos.x) - ig.game.screen.x, ig.system.getDrawPos(this.pos.y) - ig.game.screen.y, this.size.x, this.size.y );
      ctx.fillStyle = "rgba("+this.color+", 0.4)";
      ctx.closePath();
      ctx.fill();
    },

    draw : function()
    {
      if(this.state == 'notPlaced')
      {
        if(ig.game.vMap.getValue(ig.input.mouse.x + (ig.game.tile_size / 2) + ig.game.screen.x, ig.input.mouse.y + (ig.game.tile_size / 2) + ig.game.screen.y ) != -1)
        {
          this.color = '255, 0, 0';
        }else {
          this.color = '10, 10, 10';
        }
        this.setShootArea();
        this.colorTile();
      }else if( this.state == 'selected' )
      {
        this.color = '10, 10, 10';
        this.setShootArea();
        this.setUpgradeShootArea();
      }
      this.parent();
      // Set the playercolor-flag.
      // needs to be after parent, or else it will draw under the entity.
      this.drawFlag();
    },

    launchProjectile: function( target, damage, effects )
    {
      // Set settings for projectile
      var settings;
      settings = this.projectiles[0];
      settings['tower'] = this;
      settings['target'] = target;
      settings['damage'] = damage;
      settings['effects'] = effects;
      var spawnX = this.center.x - (this.projectiles[0].size.x / 2);
      var spawnY = this.center.y - (this.projectiles[0].size.y / 2);
      // Now spawn it
      ig.game.spawnEntity( EntityProjectile, spawnX, spawnY, settings );
    }
  });
});