ig.module(
  'game.entities.creep'
).requires(
  'impact.entity'
).defines(function() {

  EntityCreep = ig.Entity.extend({

    // @Param "name" : Creep name (+ number for even higher difficulty?)
    // @Param "description" : Creep description (to change on upgrade?)
    // @Param "level" : Creep Level (To calculate loot from)
    // @Param "rarity" : Creep rarity, Bronze, Silver, Gold (To calculate loot from)
    // @Param "speed" : Creep speed (Its added speed variable)
    // @Param "price" : Creep price (changes at upgrade)
    // @Param "isBoss" : Is this creep a boss
    // @Param "type" : Is it flying, is it walking, is it swimming?
    // @Param "armorType" : Light, Heavy, Like A House, and so fourth

    type : ig.Entity.TYPE.A,

    name : null,
    description : null,
    category : null,
    speed : null,
    health: null,
    directionPath : null,
    damage : 1,
    zIndex : 1, // Should be drawn first.
    activeEffects : [],
    currentDirection : 0,
    lastTile : 0,
    escaped : false,
    traversedTiles : 0,
    damageMessages : [],
    spreadTick : 0,

    init: function(x, y, settings) {

      this.parent(x, y, settings);
      // Set initial health.
      this.initialHealth = this.health;

      // Add animations.
      this.animSheet = new ig.AnimationSheet('media/creep/'+ this.sprite, this.size.x, this.size.y);
      for(var i=0;i < settings.animations.length; i++) {
        // Convert js object, to array.
        var sequence = [];
        for( var key in settings.animations[i].sequence )
        {
          sequence.push(settings.animations[i].sequence[key]);
        }
        this.addAnim( settings.animations[i].name, settings.animations[i].time, sequence );
      }
      // Push the new creep, to the activeCreeps array.
      ig.game.gameCont.activeCreeps.push(this);

      // Define path to endpoint.
      this.endpoint = ig.game.getEntitiesByType('EntityEndpoint')[0];
      this.getPath(this.endpoint.center.x, this.endpoint.center.y, false);
      this.maintainAnim();
    },


    update: function() {
      // Folllow defined path.
      this.followPath(this.speed);
      // Track tiles traversed, used for towers to set priority.
      this.trackTiles();
      // Maintain effects on creep.
      this.maintainEffects();
      // Add onDeath listener.
      this.onDeath();
      // Maintain animation according to direction.
      this.maintainAnim();
      // Call parent.
      this.parent();
    },

    trackTiles : function() {
      var currentTile = ftd.getTile( this.pos.x, this.pos.y );

      if(currentTile != this.lastTile) {
        this.lastTile = currentTile;
        this.traversedTiles++;
      }

      // Check if creep have escaped, set by game server.
      if( this.escaped )
      {
        // Check if creep have reached the endpoint.
        if(this.pos.x >= this.endpoint.pos.x
          && this.pos.x <= this.endpoint.pos.x + 40
          && this.pos.y >= this.endpoint.pos.y
          && this.pos.y <= this.endpoint.pos.y + 40)
        {
          // Kill, and remove creep from activeCreeps.
          this.kill();
          var index = ig.game.gameCont.activeCreeps.indexOf( this );
          ig.game.gameCont.activeCreeps.splice( index, 1 );
        }
      }
    },

    // Change animation, according to direction.
    maintainAnim : function() {
      // If direction is the same, no need to alter animation.
      if(this.currentDirection == this.headingDirection) {
        return;
      }
      // Set new direction.
      this.currentDirection = this.headingDirection;

      // Heading direction values.
      // 1 4 6
      // 2 0 7
      // 3 5 8
      switch(this.headingDirection) {
      case 4:
        this.currentAnim = this.anims.up;
        break;
      case 7:
        this.currentAnim = this.anims.right;
        break;
      case 5:
        this.currentAnim = this.anims.down;
        break;
      case 2:
        this.currentAnim = this.anims.left;
        break;
      }
    },

    // When the creep dies, what should happen.
    onDeath : function() {
      if(this.health <= 0) {
        // Generate start position for message.
        var pos = this.pos;
        pos.x += this.size.x / 2;
        ig.game.gameCont.addMessage("+" + this.goldBounce, pos, 30);
        // Remove creep from activeCreeps.
        this.kill();
        var index = ig.game.gameCont.activeCreeps.indexOf( this );
        ig.game.gameCont.activeCreeps.splice( index, 1 );
      }
    },

    /**
     * Effect (buffs) related code.
     */
    addEffect : function( effect )
    {
      // If this persist to be true, add the effect.
      var addEffect = true;
      // First check if there is a existent effect of same type.
      for( var i=0; i < this.activeEffects.length; i++ )
      {
        if( this.activeEffects[i].type == effect.type )
        {
          // A effect of same type is active, check if its stronger.
          if( this.activeEffects[i].value < effect.value )
          {
            // It is stronger, remove old effect.
            this.activeEffects.splice( i, 1 );
          }else {
            // It wasn't stronger than current, don't add effect.
            // TODO: Make smart, evaluate more than "value" of effect.
            addEffect = false;
          }
        }
      }

      // No effect alike is active, so add th effect.
      if( addEffect )
      {
        // Set timer, and countdown to removal.
        effect.timer = new ig.Timer( effect.time );
        effect.frequencyTimer = new ig.Timer( effect.frequency );
        this.activeEffects.push( effect );
      }
    },

    maintainEffects : function()
    {
      // Traverse each effect.
      for( var i=0; i < this.activeEffects.length; i++ )
      {
        // Handle types & apply.
        switch( this.activeEffects[i].type )
        {
          case 'slow':
            if( !this.activeEffects[i].applied )
            {
              this.oldSpeed = this.speed;
              this.speed -= this.speed / 100 * this.activeEffects[i].value;
              this.activeEffects[i].applied = true;
            }
            break;

          case 'dot':
            if( this.activeEffects[i].frequencyTimer.delta() >= 0 )
            {
              // Subtract health form creep.
              this.health -= this.activeEffects[i].value;
              // Set time until next impact.
              this.activeEffects[i].frequencyTimer.set( this.activeEffects[i].frequency );
            }
            break;
        }

        // Remove effect if timer reach zero.
        if( this.activeEffects[i].timer.delta() >= 0 )
        {
          switch( this.activeEffects[i].type )
          {
            case 'slow':
              this.speed = this.oldSpeed;
              break;
          }
          this.activeEffects.splice( i, 1 );
        }
      }
    },

    addDamageMessage : function( damage, critical )
    {
      // Clean up messages.
      var self = this;
      this.damageMessages.forEach( function( message, i )
      {
        if( message.showTimer.delta() >= 0 )
        {
          delete self.damageMessages[i];
        }
      });

      // Restart spread, if message count exceeds 4.
      if( this.spreadTick > 4)
      {
        this.spreadTick = 0;
      }

      var even = this.damageMessages.length % 2;

      var pos = {
        x : this.pos.x + (this.size.x / 2),
        y : this.pos.y
      }

      // Even the the messages out, to prevent clustering.
      if( even == 0 )
      {
        pos.x += this.spreadTick * 5;
      }
      if( even == 1 )
      {
        pos.x -= this.spreadTick * 5;
      }
      if( this.damageMessages.length >= 8 )
      {
        pos.y += 15;
        pos.x -= 10;
      }

      var message = {
        content : damage,
        critical : critical,
        pos : pos,
        showTimer : new ig.Timer( 1 ),
      };
      // Add to global messages.
      ig.game.gameCont.damageMessages.push( message );
      // Add to creeps messages, used for determine position.
      this.damageMessages.push( message );
      // Add spread.
      this.spreadTick++;
    },

    draw : function() {
      // Draw visible effects.
      this.drawDots();
      // Draw health bars.
      this.drawHealthBar();

      this.parent();
    },

    drawHealthBar: function() {
      // Draw only if creep lost health.
      if(this.health != this.initialHealth && this.health >= 0) {
        // Paint background.
        ig.system.context.fillStyle = "rgb(0,0,0)";
        ig.system.context.beginPath();
        ig.system.context.rect(
          (this.pos.x - ig.game.screen.x) * ig.system.scale + (this.size.x / 4),
          (this.pos.y - ig.game.screen.y - 8) * ig.system.scale,
          this.size.x * ig.system.scale / 2,
          3 * ig.system.scale
        );
        ig.system.context.closePath();
        ig.system.context.fill();

        // Paint health.
        ig.system.context.fillStyle = "rgb(255,0,0)";
        ig.system.context.beginPath();
        ig.system.context.rect(
          (this.pos.x - ig.game.screen.x + 1) * ig.system.scale + (this.size.x / 4 ),
          (this.pos.y - ig.game.screen.y - 7) * ig.system.scale,
          ((this.size.x - 2) * (this.health / this.initialHealth)) * ig.system.scale / 2,
          1 * ig.system.scale
        );
        ig.system.context.closePath();
        ig.system.context.fill();
      }
    },

    drawDots : function() {
      if(this.activeEffects.length > 0) {
        var ctx = ig.system.context;
        // Draw a dot, for each effect.
        for(var i=0; this.activeEffects.length > i; i++) {
          ctx.beginPath();
          ctx.arc(
            ig.system.getDrawPos( (this.pos.x - ig.game.screen.x + 1)) + ((this.size.x - ig.game.screen.x + 1)/ 2 - 5) + (i * 5),
            ig.system.getDrawPos(this.pos.y - ig.game.screen.y - 7) -12, 1, 0, Math.PI * 2
          );
          // Different color, for each type.
          if(this.activeEffects[i].type == 'dot') ctx.fillStyle = "rgb(255,0,0)";
          if(this.activeEffects[i].type == 'slow') ctx.fillStyle = "rgb(50, 85, 255)";

          ctx.closePath();
          ctx.fill();
        }
      }
    }

  });
});
