ig.module(
  'game.entities.overlay'
)
.requires(
  'impact.entity'
)
.defines( function() 
{

  EntityOverlay = ig.Entity.extend(
  {

    /**
     * @Params
     * sprite -> sprite sheet for the animation.
     * frameTime -> time between each sequence.
     * runs -> Number of times, animation should run.
    **/

    zIndex : 4,

    init : function( x, y, settings ) 
    {
      this.parent( x, y, settings );
      // Set animations sheet.
      this.animSheet = new ig.AnimationSheet( 'media/overlay/' + this.sprite, this.size.x, this.size.y );
      this.addAnim( 'animate', this.frameTime, this.seq );

      this.frames = ( (this.frameTime * this.seq.length) * 60 ) * this.runs - 10;
    },

    update : function() {

      // If runs is set to 0, keep running, animation should be killed manually.
      if( this.runs != 0 ) 
      {
        this.frames--;
        if( this.frames <= 0 ) this.kill();
      }
      this.parent();
    }

  });
});
