ig.module(
  'game.ioController'
).requires()
.defines(function() {

  IoController = ig.Class.extend({

    socket : null,
    playerId : null,
    clientId : null,
    gameId : null,


    init : function( socket, playerId, gameId ) {
      /**
       * Game initializing stuff.
       */

      // Set game id's.
      this.socket = socket;
      this.gameId = gameId;
      this.playerId = playerId;

      /**
       * Connect user to the game.
       * How room is determed, is not yet clear.
       */
      var initialize = {
        gameId : this.gameId,
        playerId : this.playerId,
      };
      this.socket.emit( 'join game', initialize, function( players, gameInfo, clientId )
      {
        self.clientId = clientId;
        self.gameInfo = gameInfo;
        // Set player overlay.
        ftd.ui.updatePlayerOverlay( players, gameInfo, false );
      });

      var self = this;

      // Retrieve players in game.
      this.socket.on('setPlayers', function( data )
      {
        ig.game.gameCont.players = data;
        // Update gold and lifes.
        ftd.ui.setPlayerStat( ftd.getPlayerPropById(self.clientId, 'life'),
                              ftd.getPlayerPropById(self.clientId, 'gold') );
        ftd.ui.updatePlayerOverlay( data, self.gameInfo, false );
      });

      // A player send a message in the chat.
      this.socket.on('message', function( data )
      {
        ftd.ui.processChatMessage( data );
      });

      this.socket.on('startGame', function( players )
      {
        // Kill player overlay.
        ftd.ui.updatePlayerOverlay( [], this.gameInfo, true );
        ig.game.gameCont.start();
        ig.game.gameCont.setStartScreen();
      });

      // Current player got disconnected from server.
      this.socket.on('disconnected', function(data) {
        // TODO
      });

      // A player joined the game.
      this.socket.on('playerCon', function(data) {
        ig.game.ioController.players.push(data);
      });

      // A player in the game disconnected.
      this.socket.on('playerDC', function(data) {
        // TODO
      });

      // Print debug data to console.
      this.socket.on('debug', function(data) {console.log(data);});

      /**
       * Game listeners.
       */

      // A player upgraded a tower.
      this.socket.on( 'upgradedTower', function( data )
      {
        // No need to continue, if tower wasn't upgraded.
        if( data.length < 1 )
        {
          return;
        }

        ig.game.gameCont.activeTowers.forEach( function( tower )
        {
          if( tower.id == data.id )
          {
            tower.upgradeTower( data );
            return;
          }
        });
      });

      // A player placed a tower.
      this.socket.on('placedTower',function(data) {
        data.state = 'placed';
        var tower = ig.game.spawnEntity(EntityTower, data.pos.x, data.pos.y, data);
        ig.game.gameCont.activeTowers.push(tower);
        // Place a "here is a tower" tile.
        ig.game.vMap.setValue(tower.pos.x, tower.pos.y + (ig.game.tile_size), 0);
      });

      // A player upgraded a tower.
      this.socket.on( 'soldTower', function( data )
      {
        ig.game.gameCont.activeTowers.forEach( function( tower )
        {
          if( tower.id == data.id )
          {
            tower.sellTower( data );
            ftd.setPlayerPropById( tower.owner.clientId, 'gold', data.sellValue);
            return;
          }
        });
      });

      this.socket.on('synchronize', function( data )
      {
        if( data.fireQueue || data.creepQueue)
        {
          // Update wave information in gui.
          ftd.ui.updateWaveInfo( data.currentWave, data.nextWave );
          if(data.creepQueue.length > 0) {
            for(var i=0; i < data.creepQueue.length; i++) {
              ig.game.gameCont.addCreep(data.creepQueue[i]);
            }
          }
          // If fireQueue is set, delegate orders to rightfull towers.
          if(data.fireQueue.length > 0) {
            var fq = data.fireQueue;
            for(var i=0; i < fq.length; i++) {
              var tower = ftd.getEntityById('tower', fq[i].tower);
              tower.fireQueue.push(fq[i]);
            }
          }
        }

        // If creeps have escaped, apply true.
        if( data.escapedCreeps )
        {
          if( data.escapedCreeps.length > 0)
          {
            var ec = data.escapedCreeps;
            for(var i=0; i < ec.length; i++) {
              var creep = ftd.getEntityById('creep', ec[i]);
              creep.escaped = true;
            }
          }
        }

        // Update players.
        if( data.players )
        {
          data.players.forEach( function( playerData )
          {
            // Find and set stats, for righful player.
            player = ftd.getPlayerByClientId( playerData.clientId );
            if( player )
            {
              player.score = playerData.score;
              player.life = playerData.life;
              player.gold = playerData.gold;
              player.kills = playerData.kills;
            }
          });
          ftd.ui.updateScoreBoard();
          ftd.ui.setPlayerStat();
        }
      });

      this.socket.on( 'finished', function( state )
      {
        if( state === true )
        {
          ftd.ui.processChatMessage( {
            sender : 'Game server',
            message : 'Game finished.'
          });
          var timer = 20;
          setInterval( function()
          {
            if( timer % 5 == 0 )
            {
              ftd.ui.processChatMessage( {
                sender : 'Game server',
                message : 'Redirecting to overview board, in ' + timer + ' seconds.'
              });
            }
            if( timer == 0 )
            {
              window.location = '/game/summary/' + self.gameId;
            }
            timer--;
          }, 1000);
        }
      });
    },

    ready : function()
    {
      var gameInfo = {
        gameId : this.gameId,
        playerId : this.playerId,
      };
      this.socket.emit('ready', gameInfo, function( player, map, gameInfo, reconnecting )
      {
        ig.game.deck = player.deck;
        ftd.ui.setTowerDeck( player.deck );
        // Load map.
        ig.game.loadLevel( eval( map ) );
        ig.game.gameCont.loadMinimap( map );
        if( reconnecting )
        {
          ftd.ui.updatePlayerOverlay( [], this.gameInfo, true );
        }
        // Boot chat.
        ftd.ui.initializeChat();
      });
    },

    // Place a tower.
    placeTower : function(tower) {
      var data = {
        playerId : this.playerId,
        clientId : this.clientId,
        gameId : this.gameId,
        name : tower.name,
        pos : tower.pos
      };
      this.socket.emit('placeTower', data);
    },

    upgradeTower : function( tower )
    {
      var data = {
        gameId : this.gameId,
        id : tower.id
      };

      this.socket.emit( 'upgradeTower', data );
    },

    sellTower : function( tower )
    {
      var data = {
        gameId : this.gameId,
        id : tower.id
      };
      this.socket.emit( 'sellTower', data );
    },

    sendChatMessage : function( message )
    {
      var data = {
        gameId : this.gameId,
        message : message
      };
      this.socket.emit( 'chatMessage', data );
    },
  });
});
