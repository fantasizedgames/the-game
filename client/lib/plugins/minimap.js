ig.module(
    'plugins.minimap'
)
.requires(
    'impact.game'
)
.defines(function(){

ig.Minimap = ig.Class.extend({
  // init objects
  minimap: {},
  minimapCenter: {},
  zIndex: 6,

  // IS LOADED IN GAMECONTROLLER INIT
  init : function( x, y, w, h, gameX, gameY, name )
  {
      // defaults
      this.minimap.x = x;
      this.minimap.y = y;
      this.minimap.width = w;
      this.minimap.height = h;

      // To draw from when placing entities on minimap..
      // These values are the divider for when drawing the correct position.
      this.scaleX = gameX / w;
      this.scaleY = gameY / h;

      this.bgWidth = gameX;
      this.bgHeight = gameY;
      // getting the right minimap Image, and remove the "Level" from level name.
      this.minimapImg = new ig.Image('media/minimap/' + name.substr(5) + '.png');
  },

  draw : function()
  { 
      // Draw the minimap background image.
      this.minimapImg.draw(this.minimap.x, this.minimap.y);
      // Draw active creeps on minimap.
      this.drawEntities( ig.game.gameCont.activeCreeps );
      // Draw active towers on minimap.
      this.drawEntities( ig.game.gameCont.activeTowers );
      // Draw active spawners on minimap.
      this.drawEntities( ig.game.getEntitiesByType(EntitySpawner) );
      // Draw active Endpoints on minimap.
      this.drawEntities( ig.game.getEntitiesByType(EntityEndpoint) );

      // If mouse is within bound of minimap, and state is pressed, you can move the minimap
      if( ig.input.state('CanvasTouch')
       && ig.input.mouse.x <= this.minimap.width + 20
       && ig.input.mouse.x >  this.minimap.x - 10
       && ig.input.mouse.y <= this.minimap.height + 20
       && ig.input.mouse.y >  this.minimap.y - 10 )
      { 
        // If mouse is in state and you are moving the border around.
        // The screen changes according to the current minimap position. 
        // ( - 10 is because of the 10px margin in minimap drawn image: top and left.
        ig.game.screen.x = ( (ig.input.mouse.x - 10) - ( (ig.system.width / this.scaleX) / 2 ) ) * this.scaleX;
        ig.game.screen.y = ( (ig.input.mouse.y - 10) - ( (ig.system.height / this.scaleY)  / 2 ) ) * this.scaleY;

        this.drawCameraBorder( ig.game.screen.x, ig.game.screen.y );
      } else {
        // Else just draw the border where screen is, ( for use in screenscrolling/camera method ).
        this.drawCameraBorder( ig.game.screen.x, ig.game.screen.y );
      }
  },

  drawEntities : function( entities )
  {
    for( var i = 0; i < entities.length; i++ )
    {
      var x = entities[i].pos.x / this.scaleX;
      var y = entities[i].pos.y / this.scaleY;

      var ctx = ig.system.context;
      // getting different entitiy types and draw rects based on type.
      if( entities[i] instanceof EntityCreep )
      {
        ctx.fillStyle="darkred";
        ctx.fillRect( x + 10, y + 10, 3, 3);
      }
      if( entities[i] instanceof EntityTower )
      {
        var color = ftd.getPlayerByClientId( entities[i].owner.clientId ).color;
        ctx.fillStyle = ftd.getRealPlayerColor(color);
        ctx.fillRect( x + 11, y + 10, 3, 6);
      }
      if( entities[i] instanceof EntitySpawner )
      {
        ctx.fillStyle="lightgreen";
        ctx.fillRect( x + 8, y + 8, 8, 8);
      }
      if( entities[i] instanceof EntityEndpoint )
      {
        ctx.fillStyle="darkred";
        ctx.fillRect( x + 9, y + 9, 5, 5);
      }
    }
  },

  drawCameraBorder : function( x, y )
  {
    // Width and height of stroked rect.
    var width  = ig.system.width / this.scaleX - 1;
    var height = ig.system.height / this.scaleY - 1;

    // screen divided by scalefactor + 10, which is the pixels the minimap is drawn from the canvas border.
    var _x = x / this.scaleX + 10;
    var _y = y / this.scaleY + 10;

    if ( _x <= this.minimap.x ) 
    {
      // border for left side of minimap.
      _x = this.minimap.x;
      // border for left side of full game map.
      ig.game.screen.x = 0;
    }
    if ( _x >= (this.minimap.width - width) + 10 ) 
    {
      // border for right side of minimap.
      _x = (this.minimap.width - width) + 10;
      // border for right side of full game map.
      ig.game.screen.x = (ig.game.collisionMap.width*ig.game.collisionMap.tilesize-ig.system.width);
    }
    if ( _y <= this.minimap.y ) 
    {
      // border for top side of minimap.
      _y = this.minimap.y;
      // border for top side of full game map.
      ig.game.screen.y = 0;
    }
    if ( _y >= (this.minimap.height - height) + 10 ) 
    {
      // border for bottom side of minimap.
      _y = (this.minimap.height - height) + 10;
      // border for bottom side of full game map.
      ig.game.screen.y = (ig.game.collisionMap.height*ig.game.collisionMap.tilesize-ig.system.height);
    } 

    // Now draw the border.
    var ctx = ig.system.context;
    ctx.beginPath();
    ctx.strokeStyle = "yellow";
    ctx.strokeRect( _x + 0.5, _y + 0.5, width, height);
    ctx.closePath();
  }
});
});
