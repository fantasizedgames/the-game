<!DOCTYPE html>
<html>
  <head>
    <title>Blomster</title>
    <script type="text/javascript" src="http://localhost:5429/socket.io/socket.io.js"></script>
    <link rel="stylesheet" type="text/css" href="style.css" />
    <script type="text/javascript" src="lib/plugins/sizzle.js"></script>
    <script type="text/javascript" src="lib/impact/impact.js"></script>
    <script type="text/javascript" src="lib/game/main.js"></script>
  </head>
  <body>
    <div id="fantasytd-wrapper">

      <div class="left-side">
        <div class="logo"></div>

        <div class="wave-info">
          <div class="current"><p>Waiting</p></div>
          <div class="next"><p>Waiting</p></div>
        </div>

          <div class="entity-info">
            <div class="tower-select">
              <div class="left">
                <div class="name" style="color: #FFFFFF;"><span class="value"></span></div>
                <div class="level" style="color: #c860e2;"><span class="value"></span></div>
                <div class="rarity" style="color: #cacaca;"><span class="value"></span></div>
                <div class="price" style="color: #f5c270;"><span class="value"></span></div>
              </div>
              <div class="right">
                <div class="damage" style="color: #d04c4c;"><span class="value"></span></div>
                <div class="range" style="color: #cac27a;"><span class="value"></span></div>
                <div class="speed" style="color: #7aca7c;"><span class="value"></span></div>
                <div class="effects" style="color: #82c7d4;"><span class="value"></span></div>
              </div>
              <div class="bottom">
                <a class="sell" href="#"></a>
                <a class="upgrade" href="#"></a>
              </div>
            </div>
          </div>

        <div class="chat">
          <div class="received">Loading..</div>
          <form id="chatForm">
            <input type="text" class="output" required></input>
            <input type="submit" disabled="disabled" class="send" value="Send"></input>
          </form>
        </div>

      </div>

      <div class="right-side">
        <div class="overlay">
          <div class="players">
            <?php for($i = 0; $i < 8; $i++): ?>
              <div class="player">
                <div class="state rotating"></div>
                <div class="name"></div>
                <div class="color"></div>
              </div>
            <?php endfor; ?>
          </div>
          <div class="game">
            <div class="info">
              <div class="name">Game: <span></span></div>
              <div class="map">Map: <span></span></div>
              <div class="limit">Players: <span></span></div>
              <div class="mode">Mode: <span></span></div>
              <div class="difficulty">Difficulty: <span></span></div>
            </div>
            <div class="image"></div>
          </div>
        </div>
        <div class="scoreboard">
          <div class="players">
            <?php for($i = 0; $i < 8; $i++): ?>
              <div class="player">
                <div class="name"></div>
                <div class="gold"></div>
                <div class="kills"></div>
                <div class="score"></div>
              </div>
            <?php endfor; ?>
          </div>     
        </div>
        <canvas id="canvas"></canvas>
        <div class="player-related">

          <div class="tower-deck">
            <div class="tower">
              <img class="tower-icon" />
              <div class="tower-name"></div>
            </div>
            <div class="tower">
              <img class="tower-icon" />
              <div class="tower-name"></div>
            </div>
            <div class="tower">
              <img class="tower-icon" />
              <div class="tower-name"></div>
            </div>
            <div class="tower">
              <img class="tower-icon" />
              <div class="tower-name"></div>
            </div>
            <div class="tower">
              <img class="tower-icon" />
              <div class="tower-name"></div>
            </div>
            <div class="tower">
              <img class="tower-icon" />
              <div class="tower-name"></div>
            </div>
          </div>

          <div class="creep-select">
            <div class="left">
            </div>
            <div class="right">
            </div>
            <div class="bottom">
            </div>
          </div>

          <div class="creep-deck"></div>

          <div class="player-stats">

            <div class="gold">
              <div class="icon"></div>
              <div class="value">872</div>
            </div>

            <div class="life">
              <div class="icon"></div>
              <div class="value">39</div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </body>
</html>
