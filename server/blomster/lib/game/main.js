exports.init = function(ig)
{

ig.module(
  'game.main'
)
.requires(
  'impact.game',
  'game.entities.creep',
  'game.entities.gameController',
  'game.entities.tower',
  'plugins.valueMap',
  'plugins.astar-for-entities',

  'game.levels.wastelands',
  'game.levels.footsteps',
  'game.levels.greatgrass',
  'game.levels.iceagony'
)
.defines( function()
{
  ig.global.MyGame = ig.Game.extend({

    tile_size : 40,
    gameCont: null,

    init: function(data) {
      ig.game.autoSort = true;
      // Hide value map
      ig.BackgroundMap.inject({
        visible:true,
        draw : function () {
          if (!this.visible) return;
          this.parent();
        }
      });
      // Save information about the game.
      this.waves = data.waves;
      this.id = data.id;
      this.map = data.map;
      // Load level, provided by the game server.
      this.loadLevel( eval( 'ig.global.' + data.map ) );
      this.gameCont = ig.game.spawnEntity(ig.global.EntityGameController, 0, 0);
    },

    loadLevel: function (level) {
      this.parent(level);

      // Add value layer to globals.
      var vl = ig.game.getMapByName('valueLayer');
      vl.visible = false;
      this.vMap = new ig.ValueMap(vl.tilesize, vl.data, false);
      this.vMap.removeCollision = false;
    }
  });
});
}