exports.init = function(ig)
{

  ig.module(
  'game.entities.gameController'
).requires(
  'impact.entity',
  'game.entities.spawner'
).defines(function()
{

  ig.global.EntityGameController = ig.Entity.extend({

    name:'gameCont',
    type: ig.Entity.TYPE.NONE,
    checkAgainst: ig.Entity.TYPE.NONE,
    collides: ig.Entity.COLLIDES.NONE,

    secTimer: null,
    curSec:0,
    curMin:0,
    prevState:null,
    currentState: 'initial',

    players : [],
    chatLog : [],
    fireQueue : [],
    creepQueue : [],
    activeTowers : [],
    activeCreeps : [],
    escapedCreeps : [],

    init : function(x,y,data){
      this.secTimer = new ig.Timer(1);
      this.spawners = ig.game.getEntitiesByType(ig.global.EntitySpawner);
      this.parent(x, y, data);
    },

    //Sets the state, and updates prevState.
    setGameState : function( targetState )
    {
      this.prevState = this.currentState;
      this.currentState = targetState;
    },

    spawnersNext : function()
    {
      // If all creeps on the map is dead, go on.
      if( this.activeCreeps.length == 0 )
      {
        if( !this.timer )
        {
          // Start a countdown to next wave.
          this.timer = new ig.Timer( 1 );
          this.countdown = 10;
        }
        if( this.timer.delta() >= 0 )
        {
          this.countdown--;
          this.timer.reset();
          console.log( ig.game.id + ' :new wave in: ' + this.countdown );
        }
      } else {
        return true;
      }

      if( this.countdown == 0 )
      {
        // If next results in false, there are no more waves.
        var next = false;
        this.spawners.forEach( function( spawner )
        {
          if( spawner.remainingWaves
              && spawner.active )
          {
            // Call next wave on each active spawner.
            // There was a wave more, set next to true, and run next wave.
            next = true;
            spawner.runNext();
          }
        });
        // kill timer.
        delete this.timer;
        return next;
      }
      // New waves is not denied, return true.
      return true;
    },

    checkGameState : function()
    {
      if( this.currentState == 'starting' )
      {
        // Start countdown for game start.
        if( !this.timer )
        {
          // Count from 10.
          this.countdown = 1;
          // Start timer.
          this.timer = new ig.Timer( 1 );
        }

        // Deliver message, every second.
        if( this.timer.delta() >= 0 )
        {
          console.log( 'Starting  first wave in '+ this.countdown );
          // Subtract from countdown, and set timer.
          this.countdown--;
          this.timer.reset();
        }

        // If countdown is finished, set running state.
        if( this.countdown <= 0 )
        {
          this.setGameState('running');
          // Remove used game timer.
          delete this.timer;
        }
      }

      if( this.currentState == 'running' )
      {
        // Check if the game was lost.
        if( this.healthPoints <= 0 )
        {
          // Game was lost, do stuff.
          this.setGameState('lost');
        }
        // Spawn next wave.
        if( !this.spawnersNext() )
        {
          // Game was won, do stuff.
          this.setGameState('won');
        }
      }
    },

    //Increments UI clock and globaltimer
    setTime : function() {

    	if(this.secTimer.delta() > 0) {
    		this.curSec ++;
    		this.secTimer.reset();
    	}
    	if (this.curSec==60) {
    		this.curSec=0;
    		this.curMin ++;
    	}
    },
    //returns a string with game clock
    getElapsedTime : function() {
    	return this.curMin +" : "+ this.curSec;
    },

    update: function()
    {
      this.checkGameState();
      this.parent();
    },
    // A player placed a tower.
    placeTower : function(tower) {
      var towerEnt = ig.game.spawnEntity(ig.global.EntityTower, tower.pos.x, tower.pos.y, tower);
      this.activeTowers.push(towerEnt);
      // Return id, for clients.
      return towerEnt.id;
    }
  });
 });
}