exports.init = function(ig)
{

  ig.module(
  'game.entities.creep'
).requires(
  'impact.entity'
).defines(function(){

  ig.global.EntityCreep = ig.Entity.extend({

    name:null,
    description: null,
    type : ig.Entity.TYPE.A,
    zIndex : -10000, // Should be drawn first.

    category:null,
    size: null,
    sprite: null,
    animations: null,
    speed : null,
    health: null,
    incomingDamage : null,
    directionPath : null,
    damage : 1,
    activeEffects : [],
    currentDirection : 0,
    lastTile : 0,
    traversedTiles : 0,

    init: function(x, y, settings) {
      this.parent(x, y, settings);

      // Push the new creep to the activeCreeps array.
      ig.game.gameCont.activeCreeps.push(this);

      // Define path to endpoint.
      this.endpoint = ig.game.getEntitiesByType('EntityEndpoint')[0];
      this.getPath(this.endpoint.center.x, this.endpoint.center.y, false);
    },

    update: function() {
      // Folllow defined path.
      this.followPath(this.speed);
      // Track tiles traversed, used for towers to set priority.
      this.trackTiles();
      // Maintain effects on creep.
      this.maintainEffects();
      // Add onDeath listener.
      this.onDeath();

      this.parent();
    },

    trackTiles : function() {
      var currentTile = ftd.getTile( ig, this.pos.x, this.pos.y );
      // Check if creep have moved to a new tile.
      if(currentTile != this.lastTile) {
        this.lastTile = currentTile;
        this.traversedTiles++;
      }
      // Check if creep have reached the endpoint.
      if(this.pos.x >= this.endpoint.pos.x
        && this.pos.x <= this.endpoint.pos.x + 40
        && this.pos.y >= this.endpoint.pos.y
        && this.pos.y <= this.endpoint.pos.y + 40)
      {
        // Kill, and remove creep from activeCreeps.
        this.kill();
        var index = ig.game.gameCont.activeCreeps.indexOf( this );
        ig.game.gameCont.activeCreeps.splice( index, 1 );
        // Add to escaped creep queue.
        ig.game.gameCont.escapedCreeps.push( this.id );
        // Subtract life from players, should be handled depending on gaming mode.
        ftd.setPlayerPropById( ig.game.gameCont, 'all', 'life', -this.damage );
      }
    },

    addEffect : function( effect )
    {
      // If this persist to be true, add the effect.
      var addEffect = true;
      // First check if there is a existent effect of same type.
      for( var i=0; i < this.activeEffects.length; i++ )
      {
        if( this.activeEffects[i].type == effect.type )
        {
          // A effect of same type is active, check if its stronger.
          if( this.activeEffects[i].value < effect.value )
          {
            // It is stronger, remove old effect.
            this.activeEffects.splice(i, 1);
          }else {
            // It wasn't stronger than current, don't add effect.
            // TODO: Make smart, evaluate more than "value" of effect.
            addEffect = false;
          }
        }
      }
      // No effect alike is active, so add th effect.
      if( addEffect )
      {
        // Set timer, and countdown to removal.
        effect.timer = new ig.Timer( effect.time );
        effect.frequencyTimer = new ig.Timer( effect.frequency );
        this.activeEffects.push( effect );
      }
    },

    maintainEffects : function()
    {
      // Traverse each effect.
      for( var i=0; i < this.activeEffects.length; i++ )
      {
        // Handle types & apply.
        switch( this.activeEffects[i].type )
        {
          case 'slow':
            if( !this.activeEffects[i].applied )
            {
              this.oldSpeed = this.speed;
              this.speed -= this.speed / 100 * this.activeEffects[i].value;
              this.activeEffects[i].applied = true;
            }
            break;

          case 'dot':
            if( this.activeEffects[i].frequencyTimer.delta() >= 0 )
            {
              // Subtract health form creep.
              this.health -= this.activeEffects[i].value;
              // Set time until next impact.
              this.activeEffects[i].frequencyTimer.set( this.activeEffects[i].frequency );
            }
            break;
        }

        // Remove effect if timer reach zero.
        if( this.activeEffects[i].timer.delta() >= 0 )
        {
          switch( this.activeEffects[i].type )
          {
            case 'slow':
              this.speed = this.oldSpeed;
              break;
          }
          this.activeEffects.splice( i, 1 );
        }
      }
    },

    onDeath : function() {
      if(this.health <= 0) {
        // Add credit to players.
        ftd.setPlayerPropById( ig.game.gameCont, this.lastHitBy, 'gold', this.goldBounce );
        ftd.setPlayerPropById( ig.game.gameCont, this.lastHitBy, 'score', 10 );

        // Append kills, and points to killer.
        ftd.setPlayerPropById(
          ig.game.gameCont,
          this.lastHitBy,
          'score',
          this.points
        );
        ftd.setPlayerPropById(
          ig.game.gameCont,
          this.lastHitBy,
          'kills',
          1
        );

        // Remove creep from activeCreeps.
        this.kill();
        var index = ig.game.gameCont.activeCreeps.indexOf( this );
        ig.game.gameCont.activeCreeps.splice( index, 1 );
      }
    }
  });
});
}