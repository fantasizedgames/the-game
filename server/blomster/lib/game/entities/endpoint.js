exports.init = function(ig)
{

ig.module(
  'game.entities.endpoint'
).requires(
  'impact.entity'
).defines(function() {
  ig.global.EntityEndpoint = ig.Entity.extend({
    type: ig.Entity.TYPE.B,
    checkAgainst : ig.Entity.TYPE.A,

    name: 'endpoint',
    size: {x: 40, y: 40},

    init: function(x, y, settings) {
      this.parent(x, y, settings);
      this.center = {x: x + this.size.x / 2, y: y + this.size.y / 2};
    },

    check : function ( creep )
    {
      // If this is called, a creep have escaped.
      // Remove the creep, life subtraction is done on the server.
      creep.kill();
      var index = ig.game.gameCont.activeCreeps.indexOf( creep );
      ig.game.gameCont.activeCreeps.splice( index, 1 );
      // Depending on active ruleset (mode), subtrack life from all player, or single.
      // @TODO: Subtract life.
    }
  });
});

}