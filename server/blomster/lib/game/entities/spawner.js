exports.init = function(ig)
{

  ig.module(
  'game.entities.spawner'
)
  .requires(
    'impact.entity','game.entities.creep'
  )
  .defines(function() {

    ig.global.EntitySpawner = ig.Entity.extend({

      name:'spawner',
      size: {x: 40, y: 40},
      waves: null,
      remainingWaves: true,
      spreadTimer: new ig.Timer(0.5),
      queready:false,
      creepQueue : [],
      nextWave : 0,
      active : false,

      type: ig.Entity.TYPE.NONE,
      checkAgainst: ig.Entity.TYPE.NONE,
      collides: ig.Entity.COLLIDES.NONE,

      /**
       * Spawner properties.
       *
       * size -> Object containing size properties,
       * waves -> Waves for the current level loaded from main,
       * creepsettings -> All creep settings loaded from main - Needs to be level specific
       * currentcreepsettings -> Settings object for the current creep beeing spawned
       * creepque -> Object Array for creep id's and spread or the wave in progress.
       * nextWave -> Integer counter for waves,
       * path -> Map specific path loaded from main - Needs work in main.
       */

      init: function(x, y, settings) {
        this.parent(x, y, settings);
        this.waves = ig.game.waves;
      },

      update: function() {
        this.spawnCreep();
        this.parent();
      },

      spawnCreep: function() {
        // Spawn from creep queue.
        if(this.creepQueue.length > 0 && this.spreadTimer.delta() >= 0) {
          var target = this.creepQueue.pop();
          this.spreadTimer = new ig.Timer(target.spread);
          var creep = ig.game.spawnEntity(ig.global.EntityCreep, this.pos.x, this.pos.y, target);
          creep.spawn = this.color;
          ig.game.gameCont.creepQueue.push(creep);
        }
      },

      // Queue creeps for wave.
      quewave: function()
      {
        // Check if any waves is left.
        if(this.remainingWaves) {
          // Pick next wave
          this.currentWave = this.waves[this.nextWave];

          // First traverse each kind of creeps, in this wave.
          for(var i=0; i < this.currentWave.creeps.length; i++) {
            // Then queue creeps, for as many defined in count property.
            for(var j=0; j < this.currentWave.creeps[i].count; j++) {
              this.creepQueue.push(this.currentWave.creeps[i]);
            }
          }
          // Reverse queue, so spawns in the right order, set in the json.
          this.creepQueue.reverse();
          // Increase nextWave counter, now that we have queued all creeps for this one.
          this.nextWave++;
          // Is there any waves left, if no, set flag.
          if(this.nextWave == this.waves.length) this.remainingWaves = false;
        }
      },

      // Used by the server script.
      getWaveInfo : function()
      {
        return {
          currentWave : this.currentWave.creeps,
          nextWave : this.waves[this.nextWave] ? this.waves[this.nextWave].creeps : [{name : ''}]
        };
      },

      //start the nextwave.
      runNext: function() {
        this.quewave();
      }
    });
  })
}
