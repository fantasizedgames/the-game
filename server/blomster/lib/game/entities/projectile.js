exports.init = function(ig)
{

  ig.module(
  'game.entities.projectile'
).requires(
  'impact.entity'
).defines(function()
{

  ig.global.EntityProjectile = ig.Entity.extend({

    // Set some of the properties
    collides: ig.Entity.COLLIDES.PASSIVE,
    maxVel: {x: 500, y: 500},
    flip: false,
    color: '#000',
    framesToKill: 5,
    deadFrames: 0,
    // zIndex at top, so that it dont flicker
    zIndex: 9999,

    /**
     * speed = time passes before hitting the target
     * sprite: sprite of projectile,
     * anim: array of animations,
     * anim.name -> name of animation
     * anim.time -> time between sprite switch
     * anim.seq -> sequence of animation
     */

    init: function( x, y, settings )
    {
      this.parent( x, y, settings );
      this.damage = Math.floor( Math.random() * this.tower.maxDamage ) + this.tower.minDamage;

      ig.game.gameCont.fireQueue.push({
        damage : this.damage,
        target : this.target.id,
        tower : this.tower.id,
        effects : this.effects
      });
      // Set incomming damage, used by towers, to determing if a target is dead, before projectile will hit.
      this.target.incomingDamage += this.damage;
    },

    update: function()
    {
      this.travel();
      this.parent();
    },

    travel : function()
    {

      var center = ftd.getCenter( this.pos, this.size );
      var pCenterX = center.x;
      var pCenterY = center.y;

      var creepCenter = ftd.getCenter( this.target.pos, this.target.size );
      var creepCenterX = creepCenter.x;
      var creepCenterY = creepCenter.y;

      var xDist = creepCenterX - pCenterX;
      var yDist = creepCenterY - pCenterY;

      var travelTime = (xDist * xDist) + (yDist * yDist);
      var cFact = 100000 / travelTime;

      this.vel.x = Math.sqrt( cFact ) * xDist;
      this.vel.y = Math.sqrt( cFact ) * yDist;

      if ( (pCenterX <= creepCenterX + 3)
           && (pCenterY <= creepCenterY + 3)
           && (pCenterX >= creepCenterX - 3)
           && (pCenterY >= creepCenterY - 3) )
      {

        // Remove health from creep.
        this.target.health -= this.damage;
        // Subtract incomming damage, as projectile have done the damage.
        this.target.incomingDamage -= this.damage;
        // Set the last hit by property, so creep entity, knows who to credit.
        this.target.lastHitBy = this.tower.owner.clientId;
        // Add projectile effects
        if( this.effects )
        {
          for( var i=0; i < this.effects.length; i++ )
          {
            this.target.addEffect(this.effects[i]);
          }
        }
        // Remove projectile, work here is done.
        this.kill();
      }
    },

    handleMovementTrace: function( res )
    {
      // IGNORE COLLISION TILES
      this.pos.x += this.vel.x * ig.system.tick;
      this.pos.y += this.vel.y * ig.system.tick;
    }
  });
});
}