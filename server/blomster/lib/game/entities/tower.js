exports.init = function(ig)
{

  ig.module(
  'game.entities.tower'
).requires(
  'impact.entity',
  'game.entities.projectile'
).defines(function(){

  ig.global.EntityTower = ig.Entity.extend({

    upgradeCount : 1,

    collides: ig.Entity.COLLIDES.PASSIVE,
    maxVel: {x: 200, y: 200},
    flip: false,
    timer : null,
    targetCreep : null,
    zIndex : 100,

    init: function( x, y, settings ) {
      this.parent( x, y, settings );
      this.timer = new ig.Timer();

      // Set center positions of tower
      this.center = ftd.getCenter( this.pos, this.size );
    },

    update: function() {
      this.pickTarget();
      this.parent();
    },

    pickTarget : function()
    {
      var creeps = ig.game.gameCont.activeCreeps;
      // No need to run the loop in every update, if tower won't shoot anyway.
      if(this.timer.delta() >= this.speed && creeps.length > 0) {
        this.timer.reset();

        // If the previous target is dead, or out of range, reset target.
        if(this.targetCreep) {
          if(this.distanceTo(this.targetCreep) >= this.range || this.targetCreep.health <= 0) {
            this.targetCreep = null;
          }
        }

        var firstPrior, secondPrior, lastPrior;
        // Traverse creeps to find a target.
        for(var i=0; i < creeps.length; i++) {
          // If the creep out of range or dead, skip it.
          if(this.distanceTo(creeps[i]) >= this.range || creeps[i].health <= 0 )
          {
            continue;
          }
          // If creep have incomming damage, that will kill it, don't waste a shot.
          if( creeps[i].health - creeps[i].incomingDamage  <= 0 )
          {
            // Last creep? No need to continue.
            if( creeps.length == 1 )
            {
              return;
            }
            continue;
          }

          // If tower don't have a target, assign one.
          if(!lastPrior) lastPrior = creeps[i];

          /**
           * Prior by tower effects.
           */

          // First, lets start by finding out if tower use effecs on projectiles
          if(this.projectiles.effects) {

            /**
             * We find all the effects types, of our tower.
             * Ex. we don't want a freeze tower, to stop shooting,
             * because creep have a dot.
             */

            var types = [];
            for(var n = 0; n < this.projectiles.effects.length; n++) {
              types.push(this.projectiles[0].effects[n].type);
            }

            // Check if creep has any of the effects this tower can apply.
            var hasEffect = false;
            for(var x = 0; x < creeps[i].activeEffects.length; x++) {
              // Also we check if the creeps affect is bigger than 30 (half second), short affects should have something to say.
              if(types.indexOf(creeps[i].activeEffects[x].type) != -1  && creeps[i].activeEffects[x].delta() < 0.5) {
                hasEffect = true;
              }
            }

            // If it did't have any of the tower effects, we set it as first prior.
            if(!hasEffect) {
              // Also we wanna target the nonaffected creep that are furthest.
              // In case firstprior haven't been set yet, we set it.
              if(!firstPrior) firstPrior = creeps[i];

              if(creeps[i].traversedTiles > firstPrior.traversedTiles) {
                firstPrior = creeps[i];
              }
            }
          }

          // Target the creep that is closest to escape.
          if(creeps[i].traversedTiles > lastPrior.traversedTiles) {
            secondPrior = creeps[i];
          }
        }
        // Target after prioty.
        if(firstPrior) this.targetCreep = firstPrior;
        else if(secondPrior) this.targetCreep = secondPrior;
        else if(lastPrior) this.targetCreep = lastPrior;

        // Fire at creep, with speed of tower.
        if(this.targetCreep) {
          this.launchProjectile(this.targetCreep);
        }
      }
    },

    upgradeTower : function()
    {
      this.upgradeCount++;
      // Rename tower, to mark upgrade count.
      if( this.upgradeCount > 2 )
      {
        this.name = this.name.replace( /\.\d+$/, '.' + this.upgradeCount );
      } else {
        this.name += ' .' + this.upgradeCount;
      }
      // Upgrade all values for tower.
      this.minDamage += Math.ceil((this.minDamage / 100) * this.upgradePercentage);
      this.maxDamage += Math.ceil((this.maxDamage / 100) * this.upgradePercentage);
      this.range += Math.ceil((this.range / 100) * this.upgradePercentage);
      this.speed -= Math.ceil((this.speed / 100) * this.upgradePercentage * 100) / 100;
      this.price += Math.ceil((this.price / 100) * this.upgradePercentage);

      // If the tower got effects on projectiles, upgrade them aswell.
      if( this.projectiles.effects )
      {
        for( var i=0; i < this.projectiles.effects.length; i++ )
        {
          this.projectiles.effects[i].value += Math.ceil( (this.projectiles.effects[i].value / 100) * this.upgradePercentage );
          this.projectiles.effects[i].time += Math.ceil( (this.projectiles.effects[i].time / 100) * this.upgradePercentage );
        }
      }
    },

    sellTower : function()
    {
      // Calculate sell value. 70% is returned to player.
      var sellValue = (this.price / 100) * 70 * this.upgradeCount;
      // Append gold to player.
      ftd.setPlayerPropById(ig.game.gameCont, this.owner.clientId, 'gold', sellValue );
      // Remove blocked tile.
      ig.game.vMap.setValue(this.pos.x + (ig.game.tile_size / 2), this.pos.y + ig.game.tile_size, -1);
      var index = ig.game.gameCont.activeTowers.indexOf( this );
      ig.game.gameCont.activeTowers.splice( index, 1 );
      this.kill();

      return sellValue;
    },

    launchProjectile: function(target) {

      // Set settings for projectile
      var settings;
      settings = this.projectiles[0];
      settings['tower'] = this;
      settings['target'] = target;

      var spawnX = this.center.x - (this.projectiles[0].size.x / 2);
      var spawnY = this.center.y - (this.projectiles[0].size.y / 2);
      // Now spawn it
      ig.game.spawnEntity( ig.global.EntityProjectile, spawnX, spawnY, settings );
    }
  });
});
}