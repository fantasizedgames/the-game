var path = require("path");

// -----------------------------------------------------------------------------
// The main() function creates the system, input, sound and game objects,
// creates a preloader and starts the run loop
exports.initGame = function initGame(ig)
{
ig.module(
  'impact.impact'
)
.requires(
  'dom.ready',
  'impact.system',
  'impact.loader'
)
.defines(function(){ //"use strict";

  ig.main = function( canvasId, gameClass, fps, scale ) {
    ig.system = new ig.System( canvasId, fps, scale || 1 );
    ig.ready = true;
    var loader = new ig.Loader( gameClass );
    loader.load();
    return ig.game;
  };
});
}