exports.init = function(ig)
{

  ig.module(
	'impact.loader'
)
.requires(
//	'impact.image',
	'impact.font',
	'impact.sound'
)
.defines(function(){ "use strict";

ig.Loader = ig.Class.extend({
	resources: [],

	gameClass: null,
	status: 0,
	done: false,

	_unloaded: [],
	_drawStatus: 0,
	_intervalId: 0,
	_loadCallbackBound: null,


	init: function( gameClass, resources ) {
		this.gameClass = gameClass;
		this.resources = resources;
		this._loadCallbackBound = this._loadCallback.bind(this);

/*
		for( var i = 0; i < this.resources.length; i++ ) {
			this._unloaded.push( this.resources[i].path );
		}

*/
	},


	load: function() {


    this.end();
    return;

    //		ig.system.clear( '#000' );

		if( !this.resources.length ) {
			this.end();
			return;
		}

		for( var i = 0; i < this.resources.length; i++ ) {
			this.loadResource( this.resources[i] );
		}
//	  	this._intervalId = setInterval(this, 16 );
	},


	loadResource: function( res ) {
		res.load( this._loadCallbackBound );
	},


	end: function() {
		if( this.done ) { return; }
		this.done = true;
		clearInterval( this._intervalId );
		ig.system.setGame( this.gameClass );
	},

	_loadCallback: function( path, status ) {
		if( status ) {
			this._unloaded.erase( path );
		}
		else {
			throw( 'Failed to load resource: ' + path );
		}

		this.status = 1 - (this._unloaded.length / this.resources.length);
		if( this._unloaded.length == 0 ) { // all done?
			setTimeout( this.end.bind(this), 250 );
		}
	}
});

});
}