var serv = require('http').createServer(handler),
io = require('socket.io').listen(serv, { log: false }),
fs = require('fs'),
mongodb = require('mongodb').MongoClient,
ObjectID = require("mongodb").ObjectID,
BSON = require("mongodb").BSONPure
sanitizer = require('sanitizer');

require('./util.js');

function handler(req, res) {
  fs.readFile(__dirname + '/index.html', function (err, data) {
    if (err) {
      res.writeHead(500);
      return res.end('Error loading index.html');
    }
    res.writeHead(200, {'Content-type': 'text/plain' });
    res.end(data);
  });
}

// Connect to mongdb.
mongodb.connect('mongodb://localhost/fantasytd', function (err, db) {

  // Save game sessions to this.
  global.sessions = {};

  io.sockets.on('connection', function(socket) {
    // A player joined the game, do initial checks.
    socket.on('join game', function( data, callback )
    {
      var gameCol = db.collection('Game');
      gameCol.find({'_id' : new ObjectID(data.gameId)}).toArray(function(err, game)
      {
        // game not found, close connection.
        if(game.length != 1) {
          socket.emit('disconnected', 'Error: game was not found.');
          socket.disconnect();
          return false;
        }

        // Pop the first array, as we only expect one result.
        game = game.pop();

        // Check if the player belongs in this game.
        var permitted = false;
        game.players.forEach(function(player) {
          if(player.oid == data.playerId) {
            permitted = true;
          }
        });
        // Player wasn't, close the connection.
        if(!permitted) {
          socket.emit('disconnected', 'Error: you are not permitted, to enter this game.');
          socket.disconnect();
        }

        // Prepare some game information.
        var gameInfo = {
          name : game.title,
          playerLimit : game.playerLimit,
          mode : game.mode,
          difficulty : game.difficulty
        };

        // Fetch all players, so waiting status can be shown.
        var i = 1;
        var players = [];
        game.players.forEach(function(player) {
          var playerCol = db.collection('Guest');
          playerCol.find({'_id' : player.oid}).toArray(function(err, playerRes) {

            players.push( {name : playerRes[0].name, state : 'loading', color : playerRes[0].color } );
            // Callback when all players are fetched.
            if(i == game.players.length)
            {
              var mapCol = db.collection( 'Map' );
              mapCol.find( {'_id' : game.map.oid} ).toArray( function( err, mapRes )
              {
                gameInfo.map = mapRes[0].title;
                // Tell client which players are loading.
                callback( players, gameInfo, socket.id );
              })
            }
            // Increment the player count.
            i++;
          });
        });
      });
    });

    socket.on( 'ready', function( data, cb )
    {
      var gameCol = db.collection( 'Game' );
      gameCol.find( {'_id' : new ObjectID( data.gameId )} ).toArray( function( err, game )
      {
        // Prepare some game information.
        var gameInfo = {
          name : game[0].title,
          playerLimit : game[0].playerLimit,
          mode : game[0].mode,
          difficulty : game[0].difficulty
        };
        // lets initialize the game, if not already done.
        if(!sessions[data.gameId]) {
          // Start by fetching the game map.
          var mapCol = db.collection( 'Map' );
          mapCol.find( {'_id' : game[0].map.oid} ).toArray( function( err, map )
          {
            // Add map to game info.
            gameInfo.map = map[0].title;
            // Save properties, will be used for new games.
            var properties = {};
            properties.map = map[0].objectTitle;
            properties.waves = game[0].wave;
            properties.id = data.gameId;

            sessions[data.gameId] = {};
            // Initialize a new instance of the impactJS engine.
            require("./blomster/lib/impact/init.js").initImpact(sessions[data.gameId]);
            // Initialize the game, this will load necessary classes etc.
            require("./blomster/lib/impact/impact.js").initGame(sessions[data.gameId].ig);
            // Finally make a new instance of our game.
            require("./blomster/lib/game/main.js").init(sessions[data.gameId].ig);
            sessions[data.gameId].ig.game = new sessions[data.gameId].ig.main( '', new sessions[data.gameId].ig.global.MyGame(properties), 30, 1, properties );
            sessions[data.gameId].ig.game.id = data.gameId;
            sessions[data.gameId].ig.game.host = data.playerId;

            // Initialize spawners.
            var spawners = sessions[data.gameId].ig.game.getEntitiesByType( sessions[data.gameId].ig.global.EntitySpawner );
            game[0].players.forEach( function( player )
            {
              // Fetch color of guest or user, type i stated in namespace.
              var playerCol = db.collection( player.namespace );
              playerCol.find( {'_id' : player.oid}, {color : 1} ).toArray( function( err, playerRes )
              {
                // Find player's belonging spawner.
                spawners.forEach( function( spawner )
                {
                  // If spawner color equals players colors, activate the spawner.
                  if( spawner.color == playerRes[0].color )
                  {
                    spawner.active = true;
                  }
                });
              });
            });
          });
        }

        // Initialize the player.
        var player = {
          state : 'ready',
          type : 'Guest',
          life : game[0].life,
          gold : game[0].gold + 100,
          score : 0,
          kills : 0,
          clientId : socket.id,
          playerId : data.playerId,
          deck : []
        };

        var reconnecting = false;
        // Check if player is already in game, maybe due to a disconnect.
        if( sessions[data.gameId] )
        {
          sessions[data.gameId].ig.game.gameCont.players.forEach(function( inGamePlayer ) {
            if( data.playerId == inGamePlayer.playerId )
            {
              // Player is reconnecting, set variable.
              reconnecting = true;
              // Player already existed, bring back persons stats.
              player = inGamePlayer;
              // But keep the new clientId, but keep old id for comparing.
              var previousPlayerId = player.clientId;
              player.clientId = socket.id;
              // Traverse all entities, for altering ownership.
              sessions[data.gameId].ig.game.gameCont.activeTowers.forEach( function( tower )
              {
                if( previousPlayerId == tower.owner.clientId )
                {
                  tower.owner.clientId = socket.id;
                }
              });
              sessions[data.gameId].ig.game.gameCont.activeCreeps.forEach( function( creep )
              {
                if( previousPlayerId == creep.lastHitBy )
                {
                  creep.lastHitBy = socket.id;
                }
              });
            }
          });
        }

        // If player is reconnecting, all data is retrieved by the previus version of the user.
        if( reconnecting )
        {
          cb( player, sessions[data.gameId].ig.game.map, gameInfo, reconnecting );
          socket.join(data.gameId);
          // Update the player array, on each client.
          io.sockets.in(data.gameId).emit('setPlayers', sessions[data.gameId].ig.game.gameCont.players);
          // Tell all clients, that a player has reconnected.
          var message = {
            sender : 'Game server',
            message : player.name + ' reconnected!'
          };
          io.sockets.in(data.gameId).emit('message', message);
          // Should force a sync here!
          return;
        }

        // Load players deck.
        var playerCol = db.collection('Guest');
        playerCol.find({'_id' : new ObjectID(data.playerId)}).toArray(function(err, playerRes) {
          // Set some player Attributes
          player.color = playerRes[0].color;
          player.name = playerRes[0].name;

          // Traverse and load each tower in deck.
          var i = 1;
          playerRes[0].deck.towers.forEach(function(tower)
          {
            var towerCol = db.collection('Tower');
            towerCol.find({'_id' : tower.oid}).toArray(function(err, completeTower) {
              player.deck.push(completeTower.pop());
              // If last tower is loaded, we can pas t
              if(i == playerRes[0].deck.towers.length) {
                // Callback, with result.
                start( player, sessions[data.gameId].ig.game.map );
              }
              i++;
            });
          });
        });

        var start = function( player, map )
        {
          // Return deck, and socket id to client.
          cb( player, map, gameInfo );
          // Add player to game. player.playerId
          sessions[data.gameId].ig.game.gameCont.players.push(player);

          // Add the player socket, to the room.
          socket.join(data.gameId);
          // Update the player array, on each client.
          io.sockets.in(data.gameId).emit('setPlayers', sessions[data.gameId].ig.game.gameCont.players);
          // Tell all clients, about the newcomer.
          var message = {
            sender : 'Game server',
            message : player.name + ' have joined in, on the party!'
          };
          io.sockets.in(data.gameId).emit('message', message );
          // If all players have now joined, start the game.
          if(sessions[data.gameId].ig.game.gameCont.players.length == game[0].players.length)
          {
            // Start the server game.
            sessions[data.gameId].ig.game.gameCont.setGameState('starting');
            // Tell clients that the game is starting.
            io.sockets.in(data.gameId).emit( 'startGame', sessions[data.gameId].ig.game.gameCont.players );
          }
        };
      });
    });


    socket.on( 'placeTower', function( data )
    {
      // Load tower players tower deck.
      var player = ftd.findClientById( data.gameId, socket.id );
      // Find the tower in players deck.
      var tower;
      player.deck.forEach( function ( deckTower )
      {
        if( deckTower.name == data.name )
        {
          tower = deckTower;
          return;
        }
      } );

      // If there wasn't found a tower, return.
      if( !tower )
      {
        return;
      }
      // Check, and remove gold from player.
      if( tower.price > player.gold )
      {
        return;
      }else {
        player.gold -= tower.price;
      }

      tower.owner = { clientId : socket.id, playerId : data.playerId };
      tower.pos = data.pos;
      tower.id = sessions[data.gameId].ig.game.gameCont.placeTower( tower );
      // Tell all players about the new tower.
      io.sockets.in( data.gameId ).emit( 'placedTower', tower );
      delete tower.id;
    } );

    socket.on( 'upgradeTower', function( data )
    {
      var tower = ftd.getEntityById( sessions[data.gameId].ig.game.gameCont, 'tower', data.id) ;

      if( socket.id == tower.owner.clientId )
      {
        // Get owner.
        var player = ftd.findClientById( data.gameId, socket.id );

        // Initialize tower upgrades.
        var upgrades = {};

        if( tower.price <= player.gold )
        {
          player.gold -= tower.price;
          tower.upgradeTower();

          // Add upgraded values to tower.
          upgrades.id = tower.id;
          upgrades.name = tower.name;
          upgrades.minDamage = tower.minDamage;
          upgrades.maxDamage = tower.maxDamage;
          upgrades.range = tower.range;
          upgrades.speed = tower.speed;
          upgrades.price = tower.price;
          // TODO: Dynamicly pick projectiles.
          upgrades.projectiles = tower.projectiles;
          // Delete target and tower, from projectile, as they are not needed.
          upgrades.projectiles.forEach(function(projectile) {
            delete projectile.tower;
            delete projectile.target;
          });
        }
        // Send upgraded tower to all clients.
        io.sockets.in( data.game ).emit( 'upgradedTower', upgrades );
      }
    } );

    socket.on('sellTower', function( data )
    {
      var tower = ftd.getEntityById( sessions[data.gameId].ig.game.gameCont, 'tower', data.id ) ;
      if( socket.id == tower.owner.clientId )
      {
        var sellValue = tower.sellTower();

        io.sockets.in( data.game ).emit( 'soldTower', {id : tower.id, sellValue : sellValue} );
      }
    });

    socket.on( 'chatMessage', function( data )
    {
      var sender = ftd.findClientById( data.gameId, socket.id );

      // If sender was found, process message.
      if( sender )
      {
        // Sanitize message.
        var message = sanitizer.sanitize( data.message );
        // Save the message to the game.
        sessions[data.gameId].ig.game.gameCont.chatLog.push({
          sender : sender,
          message : message
        })
        // Send chat message to all clients.
        message = {
          sender : socket.id,
          message : message
        }
        io.sockets.in(data.gameId).emit( 'message', message );
      }
    });
  });

  var previousPlayerData = [];
  setInterval(function () {

    Object.keys(sessions).forEach(function(gameId) {
      var doSync = false;
      var sync = {};

      // Check if there are creeps or shots, ready to be send.
      if(sessions[gameId].ig.game.gameCont.fireQueue.length > 0
         || sessions[gameId].ig.game.gameCont.creepQueue.length > 0)
      {
        // Set wave informaton.
        sync = sessions[gameId].ig.game.gameCont.spawners[0].getWaveInfo();
        // Set new targets on client games.
        sync.fireQueue = sessions[gameId].ig.game.gameCont.fireQueue;
        sync.creepQueue = sessions[gameId].ig.game.gameCont.creepQueue;
        doSync = true;
      }

      // Check if any creeps have escaped.
      if(sessions[gameId].ig.game.gameCont.escapedCreeps.length > 0 )
      {
        // Set new targets on client games.
        sync.escapedCreeps = sessions[gameId].ig.game.gameCont.escapedCreeps;
        doSync = true;
      }

      // Sync player stats.
      sync.players = [];
      sessions[gameId].ig.game.gameCont.players.forEach( function( player )
      {
        var playerData = {
          life : player.life,
          gold : player.gold,
          score : player.score,
          kills : player.kills,
          clientId : player.clientId
        }
        sync.players.push( playerData );
      });
      // If previuos player data, was the same as in last sync, no need to sync it again.
      if( !ftd.comparePlayerArrays( sync.players, previousPlayerData ) )
      {
        doSync = true;
      }
      previousPlayerData = sync.players;

      if( doSync )
      {
        // Send sync!
        io.sockets.in(gameId).emit('synchronize', sync);
      }

      // Post sync.
      if(sessions[gameId].ig.game.gameCont.fireQueue.length > 0
         || sessions[gameId].ig.game.gameCont.creepQueue.length > 0)
      {
        // Now reset the queues.
        sessions[gameId].ig.game.gameCont.fireQueue = [];
        sessions[gameId].ig.game.gameCont.creepQueue = [];
        sessions[gameId].ig.game.gameCont.escapedCreeps = [];
      }

      /**
       * React to game states.
       */

      if( sessions[gameId].ig.game.gameCont.currentState == 'won'
          || sessions[gameId].ig.game.gameCont.currentState == 'lost' )
      {
        var result = [];
        sessions[gameId].ig.game.gameCont.players.forEach( function( player )
        {
          var entry = {
            endState : sessions[gameId].ig.game.gameCont.currentState,
            lifes : player.life,
            kills : player.kills,
            score : player.score,
            player : {
              $ref : player.type,
              $id : new ObjectID( player.playerId ),
              $db : 'fantasytd',
              '_doctrine_class_name' : 1
            }
          };
          result.push( entry );
        });

        var gameCol = db.collection( 'Game' );
        gameCol.update( {'_id' : new ObjectID( gameId )},
                        {'$set' : {'results' : result }},
                        function( error, count )
        {
          delete sessions[gameId];
        });
        // Make game inactive, as it is over.
        var gameCol = db.collection( 'Game' );
        gameCol.update(
          {'_id' : new ObjectID( gameId )},
          { '$set' : {'active' : false}},
          function( error, count )
        {});
        io.sockets.in(gameId).emit( 'finished', true );
      }

    });
  }, 250);
});
console.log('Server is running at http://127.0.0.1:5429');

serv.listen(5429);
