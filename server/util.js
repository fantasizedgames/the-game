global.ftd = {

  /**
   * Entity functions.
   */

  getCenter : function( pos, size ) {
    var center = {};
    center.x = pos.x + (size.x - (size.x / 2));
    center.y = pos.y + (size.y - (size.y / 2));
    return center;
  },

  /**
   * Tiles function.
   */

  getTilePositions : function( ig, x, y, size ) {
    var tile = {};
    tile.x = (x / ig.game.tile_size).round() * ig.game.tile_size;
    tile.y = (y / ig.game.tile_size).round() * ig.game.tile_size - (size.y / 2);
    return tile;
  },

  getTile : function( ig, x, y ) {
    var xRounded = (x / ig.game.tile_size).round() * ig.game.tile_size;
    var yRounded = (y / ig.game.tile_size).round() * ig.game.tile_size;
    return xRounded + yRounded;
  },

  getValueOnTile : function( ig, x, y ) {
    // Find center, by dividing x/y with tile size.
    return ig.game.vMap.getValue(x + (ig.game.tile_size / 2), y + (ig.game.tile_size / 2));
  },

  /**
   * Find by functions.
   */

  setPlayerPropById : function( gameCont, clientId, property, value )
  {
    gameCont.players.forEach(function(player)
    {
      if(player.clientId == clientId
        || clientId == 'all')
      {
        if(property == 'gold') player.gold += value;
        if(property == 'score') player.score += value;
        if(property == 'kills') player.kills += value;
        if(property == 'life') player.life += value;
      };
    });
  },

  getEntityById : function( gameCont, entity, id )
  {
    if( entity == 'tower' )
    {
      for( var i=0; i < gameCont.activeTowers.length; i++ )
      {
        if( gameCont.activeTowers[i].id == id )
        {
          return gameCont.activeTowers[i];
        }
      }
    } else if( entity == 'creep' )
    {
      for( var i=0; i < gameCont.activeCreeps.length; i++ )
      {
        if( gameCont.activeCreeps[i].id == id )
        {
          return gameCont.activeCreeps[i];
        }
      }
    }else
    {
      return false;
    }
  },

  findClientById : function(gameId, clientId) {
    var players = sessions[gameId].ig.game.gameCont.players;
    for(var i=0; i < players.length; i++) {
      // Add tower to all other clients.
      if(players[i].clientId == clientId) {
        return players[i];
      }
    }
  },

  /**
   * Server utils
   */
  comparePlayerArrays : function( players, _players )
  {
    // If arrays are not of the same length, no need to continue.
    if( players.length != _players.length )
    {
      return false;
    }
    var equal = true;
      // Traverse, and compared each value.
    players.forEach( function( player, i )
    {
      var _player = _players[i];
      for( var key in player )
      {
        if( player[key] !== _player[key])
        {
          equal = false;
          return;
        }
      }
    });
    return equal;
  }
};
